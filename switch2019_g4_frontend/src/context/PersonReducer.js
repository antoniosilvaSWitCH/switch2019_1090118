import {
    CLEAR_TRANSACTIONS_FILTERS,
    FETCH_GROUPS_AFTER_POST,
    FETCH_GROUPS_FAILURE,
    FETCH_GROUPS_STARTED,
    FETCH_GROUPS_SUCCESS,
    FETCH_PERSON_ACCOUNTS_AFTER_POST,
    FETCH_PERSON_ACCOUNTS_FAILURE,
    FETCH_PERSON_ACCOUNTS_STARTED,
    FETCH_PERSON_ACCOUNTS_SUCCESS,
    FETCH_PERSON_CATEGORIES_AFTER_POST,
    FETCH_PERSON_CATEGORIES_FAILURE,
    FETCH_PERSON_CATEGORIES_STARTED,
    FETCH_PERSON_CATEGORIES_SUCCESS,
    FETCH_PERSON_INFO_FAILURE,
    FETCH_PERSON_INFO_STARTED,
    FETCH_PERSON_INFO_SUCCESS,
    FETCH_PERSON_TRANSACTIONS_FAILURE,
    FETCH_PERSON_TRANSACTIONS_STARTED,
    FETCH_PERSON_TRANSACTIONS_SUCCESS,
    FILTER_TRANSACTIONS,
    GROUP_ID,
    TRANSACTIONS_ACCOUNTID_FILTER,
    TRANSACTIONS_FINALDATE_FILTER,
    TRANSACTIONS_INITIALDATE_FILTER,
} from "./PersonActions";


function personReducer(state, action) {
    switch (action.type) {
        case GROUP_ID:
            return {
                ...state,
                groupId: action.groupId,
                description: action.description
            };
        case FETCH_PERSON_CATEGORIES_STARTED:
            return {
                ...state,
                categories: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_PERSON_CATEGORIES_SUCCESS:
            return {
                ...state,
                categories: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_PERSON_CATEGORIES_FAILURE:
            return {
                ...state,
                categories: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_PERSON_CATEGORIES_AFTER_POST:
            return {
                ...state,
                categories: {
                    loading: false,
                    error: null,
                    data: [],
                    isSubmitted: 'yes'
                }
            };
        case FETCH_GROUPS_STARTED:
            return {
                ...state,
                groups: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUPS_SUCCESS:
            return {
                ...state,
                groups: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUPS_FAILURE:
            return {
                ...state,
                groups: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };

        case FETCH_GROUPS_AFTER_POST:
            return {
                ...state,
                groups: {
                    loading: false,
                    error: null,
                    data: [],
                    isSubmitted: 'yes'
                }
            };

        case FETCH_PERSON_ACCOUNTS_STARTED:
            return {
                ...state,
                accounts: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_PERSON_ACCOUNTS_SUCCESS:
            return {
                ...state,
                accounts: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_PERSON_ACCOUNTS_FAILURE:
            return {
                ...state,
                accounts: {
                    loading: false,
                    error: action.payload.error,
                    data: []
                }
            };
        case FETCH_PERSON_ACCOUNTS_AFTER_POST:
            return {
                ...state,
                accounts: {
                    loading: false,
                    error: null,
                    data: [],
                    isSubmitted: 'yes'
                }
            };
        case FETCH_PERSON_TRANSACTIONS_STARTED:
            return {
                ...state,
                transactions: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_PERSON_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_PERSON_TRANSACTIONS_FAILURE:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: action.payload.error,
                    data: []
                }
            };

        //-------------------------- FILTER_TRANSACTIONS -------------------------

        case TRANSACTIONS_ACCOUNTID_FILTER:
            //Altera o accountID do transactionsFilters do estado
            return {
                ...state,
                transactionsFilters: {
                    ...state.transactionsFilters,
                    accountID: action.accountID
                }
            };
        case TRANSACTIONS_INITIALDATE_FILTER:
            //Altera o initialDate do transactionsFilters do estado
            return {
                ...state,
                transactionsFilters: {
                    ...state.transactionsFilters,
                    initialDate: action.initialDate
                }
            };
        case TRANSACTIONS_FINALDATE_FILTER:
            //Altera o finalDate do transactionsFilters do estado
            return {
                ...state,
                transactionsFilters: {
                    ...state.transactionsFilters,
                    finalDate: action.finalDate
                }
            };
        case FILTER_TRANSACTIONS:
            //Retorna o hasFilters como 'yes',
            return {
                ...state,
                transactionsFilters: {
                    ...state.transactionsFilters,
                    hasFilters: 'yes'
                }
            };
        case CLEAR_TRANSACTIONS_FILTERS:
            return {
                ...state,
                transactionsFilters: {
                    hasFilters: 'no',
                    accountID: null,
                    initialDate: '',
                    finalDate: ''
                }
            };

        //-------------------------- FETCH_PERSON_INFO -------------------------

        case FETCH_PERSON_INFO_STARTED:
            return {
                ...state,
                info: {
                    loading: true,
                    error: null,
                    data: {}
                }
            };
        case FETCH_PERSON_INFO_SUCCESS:
            return {
                ...state,
                info: {
                    loading: false,
                    error: null,
                    data: {...action.payload.data}
                }
            };
        case FETCH_PERSON_INFO_FAILURE:
            return {
                ...state,
                info: {
                    loading: false,
                    error: action.payload.error,
                    data: {}
                }
            };

        default:
            return state
    }
}

export default personReducer