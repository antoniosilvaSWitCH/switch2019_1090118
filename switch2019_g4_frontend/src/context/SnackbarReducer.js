import {FAILED_FETCH, SUCCESSFULL_FETCH} from "./SnackbarActions";

function snackbarReducer(state, action) {
  switch (action.type) {
    case SUCCESSFULL_FETCH:
      return {
        ...state,
        snackbarOpened: true,
        message: action.message,
        severity: "success"
      };
    case FAILED_FETCH:
      return {
        ...state,
        snackbarOpened: true,
        message: action.message,
        severity: "error"
      };
    case "SNACKBAR_CLEAR":
      return {
        ...state,
        snackbarOpened: false,
        message: ""
      };
    default:
      return state;
  }
}

export default snackbarReducer;
