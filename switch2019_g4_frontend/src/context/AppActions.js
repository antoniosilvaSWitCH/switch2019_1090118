export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';


export function login(username) {
  return {
    type: LOGIN,
    username: username
  }
}

export function logout() {
  return {
    type: LOGOUT,
  }
}

