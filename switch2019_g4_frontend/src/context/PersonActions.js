export const URL_API = 'http://localhost:8080/';

export const FETCH_GROUPS_STARTED = 'FETCH_GROUPS_STARTED';
export const FETCH_GROUPS_SUCCESS = 'FETCH_GROUPS_SUCCESS';
export const FETCH_GROUPS_FAILURE = 'FETCH_GROUPS_FAILURE';
export const FETCH_GROUPS_AFTER_POST = 'FETCH_GROUPS_AFTER_POST';

export function fetchGroupsStarted() {
    return {
        type: FETCH_GROUPS_STARTED,

    }
}

export function fetchGroupsSuccess(groups) {
    return {
        type: FETCH_GROUPS_SUCCESS,
        payload: {
            data:
                [...groups]
        }

    }
}

export function fetchGroupsFailure(message) {
    return {
        type: FETCH_GROUPS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchGroupsAfterPost() {
    return {
        type: FETCH_GROUPS_AFTER_POST,
    }

}

//Fetch transactions for person

export const FETCH_PERSON_TRANSACTIONS_STARTED = 'FETCH_PERSON_TRANSACTIONS_STARTED';
export const FETCH_PERSON_TRANSACTIONS_SUCCESS = 'FETCH_PERSON_TRANSACTIONS_SUCCESS';
export const FETCH_PERSON_TRANSACTIONS_FAILURE = 'FETCH_PERSON_TRANSACTIONS_FAILURE';


export function fetchPersonTransactionsStarted() {
    return {
        type: FETCH_PERSON_TRANSACTIONS_STARTED,
    }
}

export function fetchPersonTransactionsSuccess(transactions) {
    return {
        type: FETCH_PERSON_TRANSACTIONS_SUCCESS,
        payload: {
            data: [...transactions]
        }
    }
}

export function fetchPersonTransactionsFailure(message) {
    return {
        type: FETCH_PERSON_TRANSACTIONS_FAILURE,
        payload: {
            error: message
        }
    }
}

//Actions for Fetch Person Categories

export const FETCH_PERSON_CATEGORIES_STARTED = 'FETCH_PERSON_CATEGORIES_STARTED';
export const FETCH_PERSON_CATEGORIES_SUCCESS = 'FETCH_PERSON_CATEGORIES_SUCCESS';
export const FETCH_PERSON_CATEGORIES_FAILURE = 'FETCH_PERSON_CATEGORIES_FAILURE';
export const FETCH_PERSON_CATEGORIES_AFTER_POST = 'FETCH_PERSON_CATEGORIES_AFTER_POST';

export function fetchPersonCategoriesStarted() {
    return {
        type: FETCH_PERSON_CATEGORIES_STARTED,

    }
}

export function fetchPersonCategoriesSuccess(categories) {
    return {
        type: FETCH_PERSON_CATEGORIES_SUCCESS,
        payload: {
            data:
                [...categories]
        }

    }
}

export function fetchPersonCategoriesFailure(message) {
    return {
        type: FETCH_PERSON_CATEGORIES_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchPersonCategoriesAfterPost() {
    return {
        type: FETCH_PERSON_CATEGORIES_AFTER_POST,
    }
}

//-------------------------- FETCH_PERSON_ACCOUNTS -------------------------
export const FETCH_PERSON_ACCOUNTS_STARTED = 'FETCH_PERSON_ACCOUNTS_STARTED';
export const FETCH_PERSON_ACCOUNTS_SUCCESS = 'FETCH_PERSON_ACCOUNTS_SUCCESS';
export const FETCH_PERSON_ACCOUNTS_FAILURE = 'FETCH_PERSON_ACCOUNTS_FAILURE';
export const FETCH_PERSON_ACCOUNTS_AFTER_POST = 'FETCH_PERSON_ACCOUNTS_AFTER_POST';

export function fetchPersonAccountsStarted() {
    return {
        type: FETCH_PERSON_ACCOUNTS_STARTED,

    }
}

export function fetchPersonAccountsSuccess(accounts) {
    return {
        type: FETCH_PERSON_ACCOUNTS_SUCCESS,
        payload: {
            data:
                [...accounts]
        }

    }
}

export function fetchPersonAccountsFailure(message) {
    return {
        type: FETCH_PERSON_ACCOUNTS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchPersonAccountsAfterPost() {
    return {
        type: FETCH_PERSON_ACCOUNTS_AFTER_POST,
    }
}

export const GROUP_ID = 'GROUP_ID';

export function changeGroupId(groupID, description) {
    return {
        type: GROUP_ID,
        groupId: groupID,
        description: description
    }
}

//-------------------------- FILTER_TRANSACTIONS -------------------------

export const FILTER_TRANSACTIONS = 'FILTER_TRANSACTIONS';
export const TRANSACTIONS_ACCOUNTID_FILTER = 'TRANSACTIONS_ACCOUNTID_FILTER';
export const TRANSACTIONS_INITIALDATE_FILTER = 'TRANSACTIONS_INITIALDATE_FILTER';
export const TRANSACTIONS_FINALDATE_FILTER = 'TRANSACTIONS_FINALDATE_FILTER';
export const CLEAR_TRANSACTIONS_FILTERS = 'CLEAR_TRANSACTIONS_FILTERS';

export function changeTransactionsFiltersAccountID(accountID) {
    return {
        type: TRANSACTIONS_ACCOUNTID_FILTER,
        accountID: accountID
    }
}

export function changeTransactionsFiltersInitialDate(initialDate) {
    return {
        type: TRANSACTIONS_INITIALDATE_FILTER,
        initialDate: initialDate
    }
}

export function changeTransactionsFiltersFinalDate(finalDate) {
    return {
        type: TRANSACTIONS_FINALDATE_FILTER,
        finalDate: finalDate
    }
}

export function filterTransactions() {
    return {
        type: FILTER_TRANSACTIONS,
    }
}

export function clearTransactionsFilters() {
    return {
        type: CLEAR_TRANSACTIONS_FILTERS,
    }
}


//-------------------------- FETCH_PERSON_INFO -------------------------

export const FETCH_PERSON_INFO_STARTED = 'FETCH_PERSON_INFO_STARTED';
export const FETCH_PERSON_INFO_SUCCESS = 'FETCH_PERSON_INFO_SUCCESS';
export const FETCH_PERSON_INFO_FAILURE = 'FETCH_PERSON_INFO_FAILURE';

export function fetchPersonInfoStarted() {
    return {
        type: FETCH_PERSON_INFO_STARTED,
    }
}

export function fetchPersonInfoSuccess(info) {
    return {
        type: FETCH_PERSON_INFO_SUCCESS,
        payload: {
            data: {...info}
        }
    }
}

export function fetchPersonInfoFailure(message) {
    return {
        type: FETCH_PERSON_INFO_FAILURE,
        payload: {
            error: message
        }
    }
}
