export const SUCCESSFULL_FETCH = 'SUCCESSFUL_FETCH';
export const FAILED_FETCH = 'FAILED_FETCH';
export const SNACKBAR_CLEAR = 'SNACKBAR_CLEAR';

//default snackbar messages
export const GROUP_SUCCESSFULLY_CREATED = 'Group has successfully been created!';
export const CATEGORY_SUCCESSFULLY_CREATED = 'Category has successfully been created!';
export const ACCOUNT_SUCCESSFULLY_CREATED = 'Account has successfully been created!';
export const TRANSACTION_SUCCESSFULLY_CREATED = 'Transaction has successfully been created!';
export const MEMBER_SUCCESSFULLY_ADDED = 'Member has successfully been added!';
export const GROUP_CREATION_FAILED = 'Failed to create new group - ';
export const CATEGORY_CREATION_FAILED = 'Failed to create new category - ';
export const ACCOUNT_CREATION_FAILED = 'Failed to create new account - ';
export const TRANSACTION_CREATION_FAILED = 'Failed to create new transaction - ';
export const MEMBER_ADDITION_FAILED = 'Failed to add new member - ';


export function showSuccessfulSnackbar(message) {
  return {
    type: SUCCESSFULL_FETCH,
    message: message
  }
}

export function showErrorSnackbar(message) {
  return {
    type: FAILED_FETCH,
    message: message
  }
}

export function clearSnackbar() {
  return {
    type: SNACKBAR_CLEAR,
  }
}

