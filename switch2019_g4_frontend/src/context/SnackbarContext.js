import React from 'react';

const SnackbarContext = React.createContext();
export const {Provider} = SnackbarContext;
export default SnackbarContext;