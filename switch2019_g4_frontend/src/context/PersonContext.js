import React from "react";

const PersonContext = React.createContext();
export const {Provider} = PersonContext;
export default PersonContext