import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export default function RadioButtonsGroup() {
    const [value, setValue] = React.useState('mine');

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <FormControl component="fieldset">
            <FormLabel component="legend">Include transactions:</FormLabel>
            <RadioGroup aria-label="include" name="choose1" value={value} onChange={handleChange} row>
                <FormControlLabel value="mine" control={<Radio/>} label="Mine"/>
                <FormControlLabel value="disabled" disabled control={<Radio/>} label="(Mine and Mine and my groups')"/>
            </RadioGroup>
        </FormControl>
    );
}