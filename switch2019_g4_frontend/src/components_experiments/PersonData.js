import React, {useContext, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AppContext from "../context/AppContext";
import {
    fetchPersonInfoFailure,
    fetchPersonInfoStarted,
    fetchPersonInfoSuccess,
    URL_API
} from "../context/PersonActions";
import PersonContext from "../context/PersonContext";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper1: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: "white",
        backgroundColor: "#3f51b5",
        fontWeight: "bold",
    },

    paper2: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: "black",
        backgroundColor: "white",
    },

}));

export default function CenteredGrid() {
    const classes = useStyles();

    const {username} = useContext(AppContext).state;

    const {state, dispatch} = useContext(PersonContext);
    const {info} = state;
    const {data} = info;
    const rows = data;

    useEffect(() => {
        dispatch(fetchPersonInfoStarted());
        fetch(`${URL_API}/people/${username}`)
            .then(res => res.json()).then(res => dispatch(fetchPersonInfoSuccess(res)))
            .catch(err => dispatch(fetchPersonInfoFailure(err.message)))
    }, []);

    if (info.loading) {
        return (
            <div>
                <Skeleton/>
                <Skeleton animation={true}/>
                <Skeleton animation="wave"/>
            </div>
        )
    } else {
        return (
            <div className={classes.root}>
                <Grid container spacing={1}>
                    <Grid item xs={3}>
                        <Paper className={classes.paper1}>NAME</Paper>
                    </Grid>
                    <Grid item xs={3}>
                        <Paper className={classes.paper1}>ADDRESS</Paper>
                    </Grid>
                    <Grid item xs={3}>
                        <Paper className={classes.paper1}>BIRTHPLACE</Paper>
                    </Grid>
                    <Grid item xs={3}>
                        <Paper className={classes.paper1}>BIRTHDATE</Paper>
                    </Grid>
                </Grid>
                <Grid container spacing={1}>
                    <Grid item xs={3}>
                        <Paper className={classes.paper2}>{rows.name}</Paper>
                    </Grid>
                    <Grid item xs={3}>
                        <Paper className={classes.paper2}>{rows.address}</Paper>
                    </Grid>
                    <Grid item xs={3}>
                        <Paper className={classes.paper2}>{rows.birthplace}</Paper>
                    </Grid>
                    <Grid item xs={3}>
                        <Paper className={classes.paper2}>{rows.birthDate}</Paper>
                    </Grid>
                </Grid>
            </div>
        )
    }
}
