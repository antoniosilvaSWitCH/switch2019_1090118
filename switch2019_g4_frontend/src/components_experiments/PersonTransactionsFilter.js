import React, {useContext} from "react";
import PersonContext from "../context/PersonContext";
import DatePickerInline from "./DatePickerInline";
import FreeSolo from "./FreeSolo";
import Button from "@material-ui/core/Button";
import {
    changeTransactionsFiltersFinalDate,
    changeTransactionsFiltersInitialDate,
    clearTransactionsFilters,
    filterTransactions
} from "../context/PersonActions";
import moment from "moment";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        direction: "row",
        justify: "space-evenly",
        alignItems: "center"
    },
    control: {
        align: "center"
    },
}));

function PersonTransactionsFilter() {
    const {dispatch} = useContext(PersonContext);
    const classes = useStyles();

    function handleSubmit(event) {
        event.preventDefault()
        dispatch(filterTransactions())
    }

    function handleClear(event) {
        event.preventDefault();
        dispatch(clearTransactionsFilters())
        dispatch(changeTransactionsFiltersInitialDate((moment(Date()).format('YYYY-MM-DD')) + " 00:00:00"))
        dispatch(changeTransactionsFiltersFinalDate((moment(Date()).format('YYYY-MM-DD')) + " 23:59:59"))
    }

    return (
        <div>
            <Grid container className={classes.root} spacing={2}>
                <Grid item>
                    <DatePickerInline className={classes.control} id={'initialDate'} name="initialDate"
                                      label="From"/>
                </Grid>
                <Grid item>
                    <DatePickerInline className={classes.control} id={'finalDate'} name="finalDate" label="To"/>
                </Grid>
                <Grid item>
                    <FreeSolo className={classes.control} id={'accountIDFilter'} name="accountID"/>
                </Grid>
                <Grid item>
                    <Button className={classes.control} id="applyFiltersBtn" variant="contained" color="primary"
                            onClick={handleSubmit}>
                        Apply filters
                    </Button>
                </Grid>
                <Grid item>
                    <Button className={classes.control} id="clearFiltersBtn" variant="contained" color="primary"
                            onClick={handleClear}>
                        Clear filters
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default PersonTransactionsFilter