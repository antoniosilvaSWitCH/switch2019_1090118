import React, {useContext, useState} from "react";
import AppContext from "../context/AppContext";
import PersonContext from "../context/PersonContext";
import {fetchPersonCategoriesAfterPost, URL_API} from "../context/PersonActions";
import SnackbarContext from "../context/SnackbarContext";
import Input from "@material-ui/core/Input";
import {
    CATEGORY_CREATION_FAILED,
    CATEGORY_SUCCESSFULLY_CREATED,
    showErrorSnackbar,
    showSuccessfulSnackbar
} from "../context/SnackbarActions";

function FromCategories() {

    const {state} = useContext(AppContext);
    const {username} = state;
    const {dispatch} = useContext(PersonContext);
    const snackDispatch = useContext(SnackbarContext).dispatch;

    const [inputData, setInputData] = useState({
        designation: ""
    });

    function handleChange(event) {
        const {name, value} = event.target;
        setInputData(prevInputData => {
            return {
                ...prevInputData,
                [name]: value
            }
        })
    }

    function handleSubmit(event) {
        event.preventDefault();
        fetch(`${URL_API}/people/${username}/categories`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputData)
        }).then(res => res.json()).then(res => {
            res.message === undefined ?
                snackDispatch(showSuccessfulSnackbar(CATEGORY_SUCCESSFULLY_CREATED)) :
                snackDispatch(showErrorSnackbar(CATEGORY_CREATION_FAILED + res.message));
            console.log(res)
        })
            .catch(error => {
                console.log(error);
                snackDispatch(showErrorSnackbar(CATEGORY_CREATION_FAILED + error.message))
            }).then(() => {
            setInputData(prevInputData => {
                return {
                    designation: ""
                }
            });
            dispatch(fetchPersonCategoriesAfterPost())
        })
    }

    return (
        <form onSubmit={handleSubmit} id="categoryForm">
            <div className="ModalHeader">
                <p>Create new Category</p>
            </div>

            <div>
                <label className="labelForm" htmlFor="designation">Designation:</label>
                <br/>
                <Input type="text" name="designation" value={inputData.designation}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <button className="buttonForm" id="submitCategory">Submit</button>
        </form>
    )
}

export default FromCategories;