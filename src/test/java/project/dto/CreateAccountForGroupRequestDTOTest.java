package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import static org.junit.jupiter.api.Assertions.*;

class CreateAccountForGroupRequestDTOTest {

    /**
     * Test for CreateAccountForGroupRequestDTO constructor
     */
    @Test
    @DisplayName("Test for CreateAccountForPersonDTO constructor")
    void constructorHappyCaseTest() {
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        //ASSERT
        assertTrue(dto instanceof CreateAccountForGroupRequestDTO);
    }

    /**
     * Test for getAccountID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getAccountsIDs - Happy Case")
    void getAccountIDHappyCaseTest() {

        //ARRANGE
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        String expected = "1";
        //ACT
        String result = dto.getAccountID();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getGroupID - Happy Case")
    void getGroupIDHappyCaseTest() {

        //ARRANGE
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        String expected = "2";
        //ACT
        String result = dto.getGroupID();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDenomination
     * Happy case
     */
    @Test
    @DisplayName(" Test for getDenomination - Happy Case")
    void getDenominationHappyCaseTest() {

        //ARRANGE
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        String expected = "food";
        //ACT
        String result = dto.getAccountDenomination();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDescription
     * Happy case
     */
    @Test
    @DisplayName(" Test for getDescription Happy Case")
    void getDescriptionHappyCaseTest() {

        //ARRANGE
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        String expected = "food";
        //ACT
        String result = dto.getAccountDescription();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getPersonID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getPersonID Happy Case")
    void getPersonIDHappyCaseTest() {

        //ARRANGE
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        String expected = "1";
        //ACT
        String result = dto.getPersonEmail();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto1 = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        CreateAccountForGroupRequestDTO dto2 = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        CreateAccountForGroupRequestDTO dto2 = null;
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with diferent object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        GroupID groupID = new GroupID("1");
        //Act
        boolean result = dto.equals(groupID);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        //Act
        boolean result = dto.equals(dto);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false if is accountID is different
     */
    @Test
    @DisplayName("Test for equals - Ensure false if accountID is different")
    void equalsEnsureFalseWithDifferentAccountIDTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        CreateAccountForGroupRequestDTO dto2 = new CreateAccountForGroupRequestDTO("2", "2", "food", "food", "1");
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false if is groupID is different
     */
    @Test
    @DisplayName("Test for equals - Ensure false if groupID is different")
    void equalsEnsureFalseWithDifferentGroupIDTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        CreateAccountForGroupRequestDTO dto2 = new CreateAccountForGroupRequestDTO("1", "1", "food", "food", "1");
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false if is accountDenomination is different
     */
    @Test
    @DisplayName("Test for equals - Ensure false if accountDenomination is different")
    void equalsEnsureFalseWithDifferentAccountDenominationTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        CreateAccountForGroupRequestDTO dto2 = new CreateAccountForGroupRequestDTO("1", "2", "different", "food", "1");
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false if is accountDescription is different
     */
    @Test
    @DisplayName("Test for equals - Ensure false if accountDescription is different")
    void equalsEnsureFalseWithDifferentAccountDescriptionTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        CreateAccountForGroupRequestDTO dto2 = new CreateAccountForGroupRequestDTO("1", "2", "food", "different", "1");
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false if is personID is different
     */
    @Test
    @DisplayName("Test for equals - Ensure false if personID is different")
    void equalsEnsureFalseWithDifferentAccountPersonIDTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        CreateAccountForGroupRequestDTO dto2 = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "2");
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        CreateAccountForGroupRequestDTO dto = new CreateAccountForGroupRequestDTO("1", "2", "food", "food", "1");
        int expected = -1095893169;
        //Act
        int result = dto.hashCode();
        //Assert
        assertEquals(expected, result);
    }

}