package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.account.Denomination;

import static org.junit.jupiter.api.Assertions.*;

class CheckIfPeopleAreSiblingsResponseDTOTest {

    private final String isSiblingTrue = "true";
    private final String isSiblingFalse = "false";

    /**
     * Test for PeopleAreSiblingsResponseDTO constructor
     */
    @Test
    @DisplayName("PeopleAreSiblingsResponseDTO - Constructor test")
    void constructorTest() {
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingFalse);
        assertTrue(responseDTO instanceof CheckIfPeopleAreSiblingsResponseDTO);
    }

    /**
     * Test for getIsSibling method
     * Happy Case
     */
    @Test
    @DisplayName("getIsSibling - Happy Case")
    void getIsSibling1HappyCaseTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingTrue);
        String expected = "true";

        //Act
        String result = responseDTO.getIsSibling();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getIsSibling method
     * Ensure not equals
     */
    @Test
    @DisplayName("getIsSibling - Not Equals Case")
    void getIsSiblingNotEqualsTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingTrue);
        String expected = "false";

        //Act
        String result = responseDTO.getIsSibling();

        //Assert
        assertNotEquals(expected, result);
    }
    
    /**
     * Test for equals
     * Ensure true when object is the same
     */
    @Test
    @DisplayName("Equals - Ensure True With Same Object")
    void testEqualsEnsureTrueWithSameObjectTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingTrue);

        //Act
        boolean result = responseDTO.equals(responseDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false when comparing different class objects
     */
    @Test
    @DisplayName("Equals - Ensure False With Objects from different classes")
    void testEqualsEnsureFalseDifferentClassTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingTrue);
        Denomination test = new Denomination("2");

        //Act
        boolean result = responseDTO.equals(test);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when comparing with a null object
     */
    @Test
    @DisplayName("Equals - Ensure false with null Test")
    void testEqualsEnsureFalseWithNullTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingTrue);
        String test = null;

        //Act
        boolean result = responseDTO.equals(test);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Happy Case - 2 different objects with same attributes
     */
    @Test
    @DisplayName("Equals - Ensure true with different objects with same attributes")
    void testEqualsHappyCaseTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingTrue);
        CheckIfPeopleAreSiblingsResponseDTO responseDTO2 = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingTrue);

        //Act
        boolean result = responseDTO.equals(responseDTO2);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashcode
     */
    @Test
    @DisplayName("Test for HashCode")
    void testHashCode() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = new CheckIfPeopleAreSiblingsResponseDTO(isSiblingTrue);
        int expected = 3570030;

        //Act
        int result = responseDTO.hashCode();

        //Assert
        assertEquals(expected, result);
    }

}