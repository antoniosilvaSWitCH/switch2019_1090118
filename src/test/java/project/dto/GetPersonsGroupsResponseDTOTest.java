package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetPersonsGroupsResponseDTOTest {

    GetPersonsGroupsResponseDTO responseDTO;
    List<GroupDTOMinimal> groupsDTOs;
    GroupDTOMinimal groupDTO1;
    GroupDTOMinimal groupDTO2;

    @BeforeEach
    void init() {
        groupsDTOs = new ArrayList<>();
        groupDTO1 = new GroupDTOMinimal("1L", "description", "2020-01-01");
        groupDTO2 = new GroupDTOMinimal("2L", "description", "2020-01-01");
        groupsDTOs.add(groupDTO1);
        groupsDTOs.add(groupDTO2);
        responseDTO = new GetPersonsGroupsResponseDTO(groupsDTOs);
    }

    @Test
    void testToString() {
        // Arrange
        String expected = "GetPersonsGroupsResponseDTO{groups=[GroupDTOMinimal{groupID='1L', description='description', creationDate='2020-01-01'}, GroupDTOMinimal{groupID='2L', description='description', creationDate='2020-01-01'}]}";

        // Act
        String actual = responseDTO.toString();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void testEqualsEnsureTrueSameObject() {
        // Arrange
        boolean expected = true;

        // Act
        boolean actual = responseDTO.equals(responseDTO);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void testEqualsEnsureFalseDifferentAttribute() {
        // Arrange
        List<GroupDTOMinimal> groupsDTOs2 = new ArrayList<>();
        groupsDTOs2.add(groupDTO1);
        GetPersonsGroupsResponseDTO responseDTO2 = new GetPersonsGroupsResponseDTO(groupsDTOs2);

        boolean expected = false;

        // Act
        boolean actual = responseDTO.equals(responseDTO2);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void testEqualsEnsureFalseNull() {
        // Arrange
        boolean expected = false;

        // Act
        boolean actual = responseDTO.equals(null);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void testHashCode() {
        // Arrange
        int expected = responseDTO.hashCode();

        // Act
        int actual = -256801725;

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void getGroupsDTOs() {
        // Arrange
        // Act
        List<GroupDTOMinimal> actual = responseDTO.getGroupsDTOs();

        // Assert
        assertEquals(groupsDTOs, actual);

    }
}
