package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateGroupInfoDTOTest {

    private String groupID;
    private String description;
    private String creationDate;
    private String groupLedgerID;

    @BeforeEach
    void init() {
        groupID = "1L";
        description = "description";
        creationDate = "2000-01-01";
        groupLedgerID = "1L";
    }

    /**
     * Test for CreateGroupInfoDTO constructor
     */
    @Test
    void constructorTest() {
        CreateGroupInfoDTO requestDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        assertTrue(requestDTO instanceof CreateGroupInfoDTO);
    }

    /**
     * Test for getGroupID method
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String expected = "1L";

        //Act
        String result = infoDTO.getGroupID();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupID method
     * Ensure not equals
     */
    @Test
    void getGroupIDNotEqualsTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String expected = "2L";

        //Act
        String result = infoDTO.getGroupID();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getDescription method
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String expected = "description";

        //Act
        String result = infoDTO.getDescription();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getDescription method
     * Ensure not equals
     */
    @Test
    void getDescriptionNotEqualsTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String expected = "different description";

        //Act
        String result = infoDTO.getDescription();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getCreationDate method
     * Happy Case
     */
    @Test
    void getCreationDateHappyCaseTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String expected = "2000-01-01";

        //Act
        String result = infoDTO.getCreationDate();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getCreationDate method
     * Ensure not equals
     */
    @Test
    void getCreationDateNotEqualsTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String expected = "1900-01-01";

        //Act
        String result = infoDTO.getCreationDate();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getGroupLedgerID method
     * Happy Case
     */
    @Test
    void getGroupLedgerIDHappyCaseTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String expected = "1L";

        //Act
        String result = infoDTO.getGroupLedgerID();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupLedgerID method
     * Ensure not equals
     */
    @Test
    void getGroupLedgerIDNotEqualsTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String expected = "2L";

        //Act
        String result = infoDTO.getGroupLedgerID();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for equals
     * Ensure true when object is the same
     */
    @Test
    void testEqualsEnsureTrueWithSameObjectTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);

        //Act
        boolean result = infoDTO.equals(infoDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false when comparing different class objects
     */
    @Test
    void testEqualsEnsureFalseDifferentClassTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String test = "2";

        //Act
        boolean result = infoDTO.equals(test);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when comparing with a null object
     */
    @Test
    void testEqualsEnsureFalseWithNullTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String test = null;

        //Act
        boolean result = infoDTO.equals(test);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Happy Case - 2 different objects with same attributes
     */
    @Test
    void testEqualsHappyCaseTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        CreateGroupInfoDTO infoDTO2 = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);

        //Act
        boolean result = infoDTO.equals(infoDTO2);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false when groupId is different
     */
    @Test
    void testEqualsEnsureFalseDifferentGroupIDTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String differentGroupID = "5L";
        CreateGroupInfoDTO infoDTO2 = new CreateGroupInfoDTO(differentGroupID, description, creationDate, groupLedgerID);

        //Act
        boolean result = infoDTO.equals(infoDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when description is different
     */
    @Test
    void testEqualsEnsureFalseDifferentDescriptionTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String differentDescription = "different description";
        CreateGroupInfoDTO infoDTO2 = new CreateGroupInfoDTO(groupID, differentDescription, creationDate, groupLedgerID);

        //Act
        boolean result = infoDTO.equals(infoDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when creationDate is different
     */
    @Test
    void testEqualsEnsureFalseDifferentCreationDateTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String differentCreationDate = "1500-01-01";
        CreateGroupInfoDTO infoDTO2 = new CreateGroupInfoDTO(groupID, description, differentCreationDate, groupLedgerID);

        //Act
        boolean result = infoDTO.equals(infoDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when groupLedgerID is different
     */
    @Test
    void testEqualsEnsureFalseDifferentGroupLedgerIDTest() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String differentGroupLedgerID = "5L";
        CreateGroupInfoDTO infoDTO2 = new CreateGroupInfoDTO(groupID, description, creationDate, differentGroupLedgerID);

        //Act
        boolean result = infoDTO.equals(infoDTO2);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        int expected = -278024481;

        //Act
        int result = infoDTO.hashCode();

        //Assert
        assertEquals(expected, result);
    }

}