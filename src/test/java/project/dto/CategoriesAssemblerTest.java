package project.dto;

import org.junit.jupiter.api.Test;
import project.model.entities.shared.Category;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CategoriesAssemblerTest {

    /**
     * Constructor test
     */
    @Test
    void constructorTest() {
        //ARRANGE
        CategoriesAssembler assembler = new CategoriesAssembler();
        //ASSERT
        assertTrue(assembler instanceof CategoriesAssembler);
    }

    /**
     * Test for method mapToDTO
     * Happy Case
     */
    @Test
    void mapToDTOHappyCaseTest() {
        //ARRANGE
        String designation = "football";

        Set<Category> categories = new HashSet<>();
        Category category = new Category(designation);
        categories.add(category);

        CategoryDTO categoryDTO = new CategoryDTO(designation);
        Set<CategoryDTO> categoriesDTOs = new HashSet<>();
        categoriesDTOs.add(categoryDTO);

        CategoriesDTO expectedDTO = new CategoriesDTO(categoriesDTOs);

        //ACT
        CategoriesDTO actualDTO = CategoriesAssembler.mapToDTO(categories);

        //ASSERT
        assertEquals(expectedDTO, actualDTO);
    }
}