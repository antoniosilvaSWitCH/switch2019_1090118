package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GetPersonInfoResponseDTOTest {

    private String email;
    private String name;
    private String address;
    private String birthDate;
    private String birthplace;

    @BeforeEach
    void init() {
        email = "21001@switch.pt";
        name = "João";
        address = "Travessa Santa Bárbara";
        birthDate = "1987-04-17";
        birthplace = "Estarreja";
    }

    /**
     * Test for GetPersonInfoResponseDTO constructor
     */
    @Test
    @DisplayName("Test for GetPersonInfoResponseDTO constructor - Happy Case")
    void constructorTest() {
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);
        assertTrue(getPersonInfoResponseDTO instanceof GetPersonInfoResponseDTO);
    }

    /**
     * Test for getEmail method
     * <p>
     * Happy Case
     */
    @Test
    void getEmailHappyCaseTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String expected = "21001@switch.pt";

        //Act
        String result = getPersonInfoResponseDTO.getEmail();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getName method
     * <p>
     * Happy Case
     */
    @Test
    void getNameHappyCaseTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String expected = "João";

        //Act
        String result = getPersonInfoResponseDTO.getName();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getAddress method
     * <p>
     * Happy Case
     */
    @Test
    void getAddressHappyCaseTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String expected = "Travessa Santa Bárbara";

        //Act
        String result = getPersonInfoResponseDTO.getAddress();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getBirthDate method
     * <p>
     * Happy Case
     */
    @Test
    void getBirthDateHappyCaseTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String expected = "1987-04-17";

        //Act
        String result = getPersonInfoResponseDTO.getBirthDate();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getBirthplace method
     * <p>
     * Happy Case
     */
    @Test
    void getBirthplaceHappyCaseTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String expected = "Estarreja";

        //Act
        String result = getPersonInfoResponseDTO.getBirthplace();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * <p>
     * Happy Case - Comparing same object
     */
    @Test
    void testEqualsHappyCaseTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        //Act
        boolean result = getPersonInfoResponseDTO.equals(getPersonInfoResponseDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * <p>
     * Happy Case - 2 different GetPersonInfoResponseDTO  objects with same attributes
     */
    @Test
    void testEqualsHappyCaseDifferentObjectsSameAttributesTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        GetPersonInfoResponseDTO otherGetPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        //Act
        boolean result = getPersonInfoResponseDTO.equals(otherGetPersonInfoResponseDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if one of the objects is null
     */
    @Test
    void testEqualsEnsureFalseIfOtherIsNullTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        GetPersonInfoResponseDTO otherGetPersonInfoResponseDTO = null;

        //Act
        boolean result = getPersonInfoResponseDTO.equals(otherGetPersonInfoResponseDTO);

        //Assert
        assertFalse(result);

    }

    /**
     * Test for equals
     * <p>
     * Ensure false if class is different
     */
    @Test
    void testEqualsEnsureFalseIfClassIsDifferentTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String other = "other";

        //Act
        boolean result = getPersonInfoResponseDTO.equals(other);

        //Assert
        assertFalse(result);

    }

    /**
     * Test for equals
     * <p>
     * Ensure false if name is different
     */
    @Test
    void testEqualsEnsureFalseDifferentNameTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String otherName = "Marlene";
        GetPersonInfoResponseDTO otherGetPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, otherName,
                address, birthDate, birthplace);

        //Act
        boolean result = getPersonInfoResponseDTO.equals(otherGetPersonInfoResponseDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if address is different
     */
    @Test
    void testEqualsEnsureFalseDifferentAddressTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String otherAddress = "Rua do Amial";
        GetPersonInfoResponseDTO otherGetPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                otherAddress, birthDate, birthplace);

        //Act
        boolean result = getPersonInfoResponseDTO.equals(otherGetPersonInfoResponseDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if birthDate is different
     */
    @Test
    void testEqualsEnsureFalseDifferentBirthDateTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String otherBirthDate = "1987-04-18";
        GetPersonInfoResponseDTO otherGetPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, otherBirthDate, birthplace);

        //Act
        boolean result = getPersonInfoResponseDTO.equals(otherGetPersonInfoResponseDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if birthplace is different
     */
    @Test
    void testEqualsEnsureFalseDifferentBirthPlaceTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String otherBirthPlace = "Porto";
        GetPersonInfoResponseDTO otherGetPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, otherBirthPlace);

        //Act
        boolean result = getPersonInfoResponseDTO.equals(otherGetPersonInfoResponseDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if email is different
     */
    @Test
    void testEqualsEnsureFalseDifferentEmailTest() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        String otherEmail = "21002@switch.pt";
        GetPersonInfoResponseDTO otherGetPersonInfoResponseDTO = new GetPersonInfoResponseDTO(otherEmail, name,
                address, birthDate, birthplace);

        //Act
        boolean result = getPersonInfoResponseDTO.equals(otherGetPersonInfoResponseDTO);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        GetPersonInfoResponseDTO getPersonInfoResponseDTO = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);
        int expected = -1162048348;

        //Act
        int result = getPersonInfoResponseDTO.hashCode();

        //Assert
        assertEquals(expected, result);

    }
}