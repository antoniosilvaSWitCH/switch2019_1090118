package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;
import project.dto.AddMemberInfoDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetPeopleByNameRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    @Test
    void getPeopleByNameHappyCaseTest() throws Exception {
        String personID = "1001@switch.pt";
        String name = "el";

        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", personID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/filter")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        //Arrange
        JSONObject expectedContent = new JSONObject()
                .put("membersIDs", new JSONArray(Arrays.asList("10002@switch.pt", "8101@switch.pt", "1001@switch.pt", "1003@switch.pt", "111@switch.pt")))
                .put("names", new JSONArray(Arrays.asList("Miguel", "Miguel", "Manuel", "Daniela", "Isabel")));
        int expectedStatus = 200;

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("name", name))
                .andReturn();
        String actualContent = mvcResult.getResponse().getContentAsString();
        int actualStatus = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(actualContent, expectedContent, true);

    }

    @Test
    public void getPeopleByNamePersonNotFoundTest() throws Exception {
        String personID = "1@switch.pt";
        String name = "el";

        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", personID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/filter")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Person not found")
                .put("errors", new JSONArray(Collections.singletonList("Person not found")));

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("name", name))
                .andReturn();
        String actualContent = mvcResult.getResponse().getContentAsString();
        int actualStatus = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(actualContent, expectedContent, true);
    }

}