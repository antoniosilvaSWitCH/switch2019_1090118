package project.controllers.web;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.*;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.AccountConflictException;
import project.exceptions.AccountNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSDuplicateAccountService;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class DuplicateAccountRESTControllerTest {

    @Autowired
    private DuplicateAccountRESTController controller;

    @Autowired
    private IUSDuplicateAccountService service;

    @Test
    public void duplicateAccountHappyCaseTest() {

        String personID = "1001@switch.pt";
        String accountID = "1001";
        String newAccountID = "1011";

        DuplicateAccountRequestDTO requestDTO = DuplicateAccountAssembler.mapToRequestDTO(personID, accountID, newAccountID);
        CreateAccountForPersonResponseDTO expected = DuplicateAccountAssembler.mapToResponseDTO(personID, "Manuel", newAccountID, "comida e bebida");

        Mockito.when(service.duplicateAccount(requestDTO)).thenReturn(expected);

        //ACT
        Object actual = controller.duplicateAccount(personID, accountID, newAccountID).getBody();

        //ASSERT
        assertEquals(expected, actual);
    }

    @Test
    public void duplicateAccountPersonNotFoundTest() {

        String personID = "1@switch.pt";
        String accountID = "1001";
        String newAccountID = "1011";

        DuplicateAccountRequestDTO requestDTO = DuplicateAccountAssembler.mapToRequestDTO(personID, accountID, newAccountID);

        Mockito.when(service.duplicateAccount(requestDTO)).thenThrow(PersonNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            controller.duplicateAccount(personID, accountID, newAccountID);
        });
    }

    @Test
    public void duplicateAccountAccountNotFoundTest() {

        String personID = "1001@switch.pt";
        String accountID = "1";
        String newAccountID = "1011";

        DuplicateAccountRequestDTO requestDTO = DuplicateAccountAssembler.mapToRequestDTO(personID, accountID, newAccountID);

        Mockito.when(service.duplicateAccount(requestDTO)).thenThrow(AccountNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            controller.duplicateAccount(personID, accountID, newAccountID);
        });
    }

    @Test
    public void duplicateAccountAccountNotOfUserTest() {

        String personID = "1001@switch.pt";
        String accountID = "1001";
        String newAccountID = "8001";

        DuplicateAccountRequestDTO requestDTO = DuplicateAccountAssembler.mapToRequestDTO(personID, accountID, newAccountID);

        Mockito.when(service.duplicateAccount(requestDTO)).thenThrow(AccountConflictException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountConflictException.class, () -> {
            controller.duplicateAccount(personID, accountID, newAccountID);
        });
    }

    @Test
    public void duplicateAccountNewAccountAlreadyExistsTest() {

        String personID = "1001@switch.pt";
        String accountID = "1001";
        String newAccountID = "1003";

        DuplicateAccountRequestDTO requestDTO = DuplicateAccountAssembler.mapToRequestDTO(personID, accountID, newAccountID);

        Mockito.when(service.duplicateAccount(requestDTO)).thenThrow(AccountAlreadyExistsException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            controller.duplicateAccount(personID, accountID, newAccountID);
        });
    }


}
