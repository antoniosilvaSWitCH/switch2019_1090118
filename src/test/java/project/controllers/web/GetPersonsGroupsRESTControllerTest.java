package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GetPersonsGroupsResponseDTO;
import project.dto.GroupDTOMinimal;
import project.frameworkddd.IUSGetPersonsGroupsService;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetPersonsGroupsRESTControllerTest extends AbstractTest {

    @Autowired
    private IUSGetPersonsGroupsService service;

    @Autowired
    private GetPersonsGroupsRESTController controller;

    @Test
    @DisplayName("Test for getPersonsGroups - Happy Case")
    public void getPersonsGroupsHappyCaseTest() {
        // Arrange
        List<GroupDTOMinimal> expectedString = new ArrayList<>();
        expectedString.add(new GroupDTOMinimal("1", "description", "creationDate"));
        GetPersonsGroupsResponseDTO outputDTOExpected = new GetPersonsGroupsResponseDTO(expectedString);

        String personID = "1@switch.pt";
        Mockito.when(service.getPersonsGroups(personID)).thenReturn(outputDTOExpected);

        //ACT
        Object outputDTO = controller.getPersonsGroups(personID).getBody();

        //ASSERT
        assertEquals(outputDTOExpected, outputDTO);
    }

    @Test
    public void getPersonsGroupsBeforeDateHappyCaseTest() {
        // Arrange
        List<GroupDTOMinimal> expectedString = new ArrayList<>();
        expectedString.add(new GroupDTOMinimal("1", "description", "creationDate"));
        GetPersonsGroupsResponseDTO outputDTOExpected = new GetPersonsGroupsResponseDTO(expectedString);

        String personID = "1@switch.pt";
        String date = "2019-12-18";
        Mockito.when(service.getPersonsGroupsBeforeDate(personID, date)).thenReturn(outputDTOExpected);

        //ACT
        Object outputDTO = controller.getPersonsGroups(personID).getBody();

        //ASSERT
        assertEquals(outputDTOExpected, outputDTO);
    }

}