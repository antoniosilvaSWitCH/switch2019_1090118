package project.controllers.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.dto.CreateGroupInfoDTO;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CreateGroupRESTControllerIT extends AbstractTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Autowired
    GroupRepository groupRepository;

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * Integration Test for createGroup of CreateGroupRESTController class
     * Happy Case
     */
    @Test
    @DisplayName("Test for createGroup - Happy Case")
    public void createGroupHappyCaseTest() throws Exception {
        //Arrange
        String uri = "/people/21001@switch.pt/groups";

        final String groupID = "21017";
        final String description = "ThisIsADescription";
        final String creationDate = "2020-01-01";
        final String groupLedgerID = "21037";

        CreateGroupInfoDTO createGroupInfoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String inputJson = super.mapToJson(createGroupInfoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).
                content(inputJson)).andReturn();

        String expectedContent = "{\"groupID\":\"21017\",\"description\":\"ThisIsADescription\",\"_links\":{\"self\":{\"href\":\"" +
                "http://localhost/groups/21017\"}}}";

        //Act
        int actualStatus = mvcResult.getResponse().getStatus();
        String actualContent = mvcResult.getResponse().getContentAsString();

        //Assert
        assertEquals(201, actualStatus);
        assertEquals(expectedContent, actualContent);
    }

    /**
     * Integration Test for createGroup method of CreateGroupRESTController class
     * Ensure error 422 when group requested to be created already existed in group ID repository
     *
     * @throws Exception
     */
    @Test
    @DisplayName("Ensure error 422 when group requested to be created already existed in group ID repository")
    public void createGroupEnsureErrorWhenGroupAlreadyExistedTest() throws Exception {
        //Arrange
        String uri = "/people/21001@switch.pt/groups";

        final String groupID = "21011";
        final String description = "ThisIsADescription";
        final String creationDate = "2020-01-01";
        final String groupLedgerID = "21036";

        CreateGroupInfoDTO createGroupInfoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String inputJson = super.mapToJson(createGroupInfoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).
                content(inputJson)).andReturn();

        String expectedContent = "{\"status\":\"UNPROCESSABLE_ENTITY\",\"message\":\"Group already exists\",\"errors\":" +
                "[\"Group already exists\"]}";

        //Act
        int actualStatus = mvcResult.getResponse().getStatus();
        String actualContent = mvcResult.getResponse().getContentAsString();

        //Assert
        assertEquals(422, actualStatus);
        assertEquals(expectedContent, actualContent);
    }

    /**
     * Integration Test for createGroup method of CreateGroupRESTController class
     * Ensure error 422 when group's ledger ID requested to be created already existed in ledger repository
     *
     * @throws Exception
     */
    @Test
    @DisplayName("Ensure error 422 when group's ledger ID requested to be created already existed in ledger repository")
    public void createGroupEnsureErrorWhenGroupLedgerAlreadyExistedTest() throws Exception {
        //Arrange
        String uri = "/people/21001@switch.pt/groups";

        final String groupID = "21016";
        final String description = "ThisIsADescription";
        final String creationDate = "2020-01-01";
        final String groupLedgerID = "21031";

        CreateGroupInfoDTO createGroupInfoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String inputJson = super.mapToJson(createGroupInfoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String expectedContent = "{\"status\":\"UNPROCESSABLE_ENTITY\",\"message\":\"Group ledger already exists\",\"" +
                "errors\":[\"Group ledger already exists\"]}";

        //Act
        int actualStatus = mvcResult.getResponse().getStatus();
        String actualContent = mvcResult.getResponse().getContentAsString();

        //Assert
        assertEquals(422, actualStatus);
        assertEquals(expectedContent, actualContent);
    }

    /**
     * Integration Test for createGroup method of CreateGroupRESTController class
     * Ensure error 422 when creator ID doesn't exist in person repository
     *
     * @throws Exception
     */
    @Test
    @DisplayName("Ensure error 422 when creator ID doesn't exist in person repository")
    public void createGroupEnsureErrorWhenCreatorDoesNotExistInPersonRepoTest() throws Exception {
        //Arrange
        String uri = "/people/21009@switch.pt/groups";

        final String groupID = "21015";
        final String description = "ThisIsADescription";
        final String creationDate = "2020-01-01";
        final String groupLedgerID = "21035";

        CreateGroupInfoDTO createGroupInfoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        String inputJson = super.mapToJson(createGroupInfoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        String expectedContent = "{\"status\":\"NOT_FOUND\",\"message\":\"Person not found\",\"errors\":" +
                "[\"Person not found\"]}";

        //Act
        int actualStatus = mvcResult.getResponse().getStatus();
        String actualContent = mvcResult.getResponse().getContentAsString();

        //Assert
        assertEquals(404, actualStatus);
        assertEquals(expectedContent, actualContent);
    }

    /**
     * getGroupByID
     * Happy Case
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("getGroupByID - HappyCase")
    public void getGroupByIDHappyCaseTest() throws Exception {
        //Arrange
        String uri = "/groups/21011";
        String expectedContent = "{\"members\":[\"21001@switch.pt\"],\"managers\":[\"21001@switch.pt\"],\"groupID\":\"21011\",\"ledgerID\":" +
                "\"21031\",\"description\":\"Switchadas\",\"creationDate\":\"1987-04-17\"}";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String actualContent = mvcResult.getResponse().getContentAsString();

        //Assert
        assertEquals(200, status);
        assertEquals(expectedContent, actualContent);
    }
}
