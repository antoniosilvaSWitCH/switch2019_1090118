package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;
import project.dto.CopyCategoryBetweenGroupsInfoDTO;
import project.dto.CreateAccountForGroupInfoDTO;
import project.dto.CreateCategoryInfoDTO;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CopyCategoryBetweenGroupsRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    @Test
    public void copyCategoryBetweenGroupsHappyCaseTest() throws Exception {
        //ArrangePOST
        String uriPOST = "/people/100001@switch.pt/groups/copy/category";

        String groupIdA = "1000011";
        String groupIdB = "1000012";
        String designation = "Material";

        CopyCategoryBetweenGroupsInfoDTO infoDTO = new CopyCategoryBetweenGroupsInfoDTO(groupIdA, groupIdB, designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResultPOST = mvc.perform(MockMvcRequestBuilders.post(uriPOST).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        //ActPOST
        int statusPOST = mvcResultPOST.getResponse().getStatus();
        String contentPOST = mvcResultPOST.getResponse().getContentAsString();

        //AssertPOST
        assertEquals(201, statusPOST);
        assertEquals("{\"groupID\":\"1000012\",\"groupDescription\":\"Empresa\",\"newCategory\":\"Material\",\"_links\":{\"self\":{\"href\":\"http://localhost/groups/1000012/categories\"}}}", contentPOST);

        //ArrangeGET
        String uriGET = "/groups/1000012/categories";

        MvcResult mvcResultGET = mvc.perform(MockMvcRequestBuilders.get(uriGET).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        //ActGET
        int statusGET = mvcResultGET.getResponse().getStatus();
        String contentGET = mvcResultGET.getResponse().getContentAsString();

        //AssertGET
        assertEquals(200, statusGET);
        assertEquals("{\"categories\":[{\"designation\":\"Material\"}]}", contentGET);

    }

    @Test
    public void copyCategoryBetweenGroupsEnsureExceptionPersonNotFoundTest() throws Exception {
        //ArrangePOST
        String uriPOST = "/people/1@switch.pt/groups/copy/category";

        String groupIdA = "1000011";
        String groupIdB = "1000012";
        String designation = "Material";

        String expected = "{\"status\":\"NOT_FOUND\",\"message\":\"Person not found\",\"errors\":[\"Person not found\"]}";

        CopyCategoryBetweenGroupsInfoDTO infoDTO = new CopyCategoryBetweenGroupsInfoDTO(groupIdA, groupIdB, designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uriPOST).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

    @Test
    public void copyCategoryBetweenGroupsEnsureExceptionGroupANotFoundTest() throws Exception {
        //ArrangePOST
        String uriPOST = "/people/100001@switch.pt/groups/copy/category";

        String groupIdA = "1";
        String groupIdB = "1000012";
        String designation = "Material";

        String expected = "{\"status\":\"NOT_FOUND\",\"message\":\"Group not found\",\"errors\":[\"Group not found\"]}";

        CopyCategoryBetweenGroupsInfoDTO infoDTO = new CopyCategoryBetweenGroupsInfoDTO(groupIdA, groupIdB, designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uriPOST).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

    @Test
    public void copyCategoryBetweenGroupsEnsureExceptionGroupBNotFoundTest() throws Exception {
        //ArrangePOST
        String uriPOST = "/people/100001@switch.pt/groups/copy/category";

        String groupIdA = "1000011";
        String groupIdB = "1";
        String designation = "Material";

        String expected = "{\"status\":\"NOT_FOUND\",\"message\":\"Group not found\",\"errors\":[\"Group not found\"]}";

        CopyCategoryBetweenGroupsInfoDTO infoDTO = new CopyCategoryBetweenGroupsInfoDTO(groupIdA, groupIdB, designation);
        String inputJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uriPOST).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(expected, content);
    }

    @Test
    public void copyCategoryBetweenGroupsEnsureExceptionPersonIsNotManagerOfGroupATest() throws Exception {
        String personID = "100001@switch.pt";
        String groupIdA = "8101";
        String groupIdB = "1000012";
        String designation = "Material";

        //ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", personID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups/copy/category")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 403;
        JSONObject expectedContent = new JSONObject()
                .put("status", "FORBIDDEN")
                .put("message", "Person is not manager of the group")
                .put("errors", new JSONArray(Collections.singletonList("Person is not manager of the group")));

        CopyCategoryBetweenGroupsInfoDTO infoDTO = new CopyCategoryBetweenGroupsInfoDTO(groupIdA, groupIdB, designation);
        String inputJson = super.mapToJson(infoDTO);

        //ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);

    }

    @Test
    public void copyCategoryBetweenGroupsEnsureExceptionPersonIsNotManagerOfGroupBTest() throws Exception {
        String personID = "100001@switch.pt";
        String groupIdA = "1000011";
        String groupIdB = "8101";
        String designation = "Material";

        //ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", personID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups/copy/category")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 403;
        JSONObject expectedContent = new JSONObject()
                .put("status", "FORBIDDEN")
                .put("message", "Person is not manager of the group")
                .put("errors", new JSONArray(Collections.singletonList("Person is not manager of the group")));

        CopyCategoryBetweenGroupsInfoDTO infoDTO = new CopyCategoryBetweenGroupsInfoDTO(groupIdA, groupIdB, designation);
        String inputJson = super.mapToJson(infoDTO);

        //ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);

    }

    @Test
    public void copyCategoryBetweenGroupsEnsureExceptionGroupADoesNotHaveCategoryTest() throws Exception {
        String personID = "100001@switch.pt";
        String groupIdA = "1000011";
        String groupIdB = "1000012";
        String designation = "Festa";

        //ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", personID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups/copy/category")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Category not found")
                .put("errors", new JSONArray(Collections.singletonList("Category not found")));

        CopyCategoryBetweenGroupsInfoDTO infoDTO = new CopyCategoryBetweenGroupsInfoDTO(groupIdA, groupIdB, designation);
        String inputJson = super.mapToJson(infoDTO);

        //ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);

    }

}
