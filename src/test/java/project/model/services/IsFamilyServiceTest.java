package project.model.services;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import project.ProjectApplication;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IsFamilyServiceTest {

    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    LedgerRepository ledgerRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    IsFamilyService service;

    Group groupA;
    Group groupB;
    Group groupC;
    Group groupD;
    Group groupE;
    Group groupF;
    Group groupG;
    Group groupH;
    Group groupI;
    Group groupJ;

    @BeforeAll
    public void init() {
        String address = "Rua Santa Catarina 35";
        String birthPlace = "Porto";
        String birthDate = "1989-12-18";
        String creationDate = "2019-12-18";
        String description = "general";
        //Group A - Family Group
        PersonID carlosID = new PersonID("4001@switch.pt");
        LedgerID carlosLedgerID = new LedgerID("4001");
        PersonID carlaID = new PersonID("4002@switch.pt");
        LedgerID carlaLedgerID = new LedgerID("4002");
        PersonID carlitosID = new PersonID("4003@switch.pt");
        LedgerID carlitosLedgerID = new LedgerID("4003");
        PersonID mariaID = new PersonID("4004@switch.pt");
        LedgerID mariaLedgerID = new LedgerID("4004");
        GroupID groupAID = new GroupID("4501");
        LedgerID groupALedgerID = new LedgerID("4501");

        //Group B - Not family group
        PersonID joaoID = new PersonID("4005@switch.pt");
        LedgerID joaoLedgerID = new LedgerID("4005");
        PersonID carlaoID = new PersonID("4006@switch.pt");
        LedgerID carlaoLedgerID = new LedgerID("4006");
        PersonID carlotaID = new PersonID("4007@switch.pt");
        LedgerID carlotaLedgerID = new LedgerID("4007");
        GroupID groupBID = new GroupID("4502");
        LedgerID groupBLedgerID = new LedgerID("4502");

        //Group C - Not family group
        GroupID groupCID = new GroupID("4503");
        LedgerID groupCLedgerID = new LedgerID("4503");

        //Group D - Not family group
        GroupID groupDID = new GroupID("4504");
        LedgerID groupDLedgerID = new LedgerID("4504");

        //Group E - Not family group
        GroupID groupEID = new GroupID("4505");
        LedgerID groupELedgerID = new LedgerID("4505");

        //Group F - Not family group
        GroupID groupFID = new GroupID("4506");
        LedgerID groupFLedgerID = new LedgerID("4506");

        //Group G - Family group
        PersonID patriciaID = new PersonID("4008@switch.pt");
        LedgerID patriciaLedgerID = new LedgerID("4008");
        PersonID catarinaID = new PersonID("4009@switch.pt");
        LedgerID catarinaLedgerID = new LedgerID("4009");
        GroupID groupGID = new GroupID("4507");
        LedgerID groupGLedgerID = new LedgerID("4507");

        //Group H - Family group
        PersonID pedroID = new PersonID("4010@switch.pt");
        LedgerID pedroLedgerID = new LedgerID("4010");
        PersonID carinaID = new PersonID("4011@switch.pt");
        LedgerID carinaLedgerID = new LedgerID("4011");
        GroupID groupHID = new GroupID("4508");
        LedgerID groupHLedgerID = new LedgerID("4508");

        //Group I - Not Family group
        PersonID catiaID = new PersonID("4012@switch.pt");
        LedgerID catiaLedgerID = new LedgerID("4012");
        GroupID groupIID = new GroupID("4509");
        LedgerID groupILedgerID = new LedgerID("4509");

        //Group J - Not Family group
        GroupID groupJID = new GroupID("4510");
        LedgerID groupJLedgerID = new LedgerID("4510");

        groupA = new Group(groupAID, description, creationDate, carlosID, groupALedgerID);
        groupA.addMemberID(carlaID);
        groupA.addMemberID(carlitosID);
        groupA.addMemberID(mariaID);
        groupB = new Group(groupBID, description, creationDate, carlosID, groupBLedgerID);
        groupB.addMemberID(carlaID);
        groupB.addMemberID(carlaoID);
        groupB.addMemberID(carlotaID);
        groupC = new Group(groupCID, description, creationDate, carlitosID, groupCLedgerID);
        groupD = new Group(groupDID, description, creationDate, carlosID, groupDLedgerID);
        groupD.addMemberID(carlitosID);
        groupD.addMemberID(mariaID);
        groupE = new Group(groupEID, description, creationDate, carlaID, groupELedgerID);
        groupE.addMemberID(carlitosID);
        groupE.addMemberID(mariaID);
        groupF = new Group(groupFID, description, creationDate, carlitosID, groupFLedgerID);
        groupF.addMemberID(mariaID);
        groupG = new Group(groupGID, description, creationDate, carlaID, groupGLedgerID);
        groupG.addMemberID(carlosID);
        groupG.addMemberID(carlitosID);
        groupG.addMemberID(catarinaID);
        groupH = new Group(groupHID, description, creationDate, carlaID, groupHLedgerID);
        groupH.addMemberID(carlosID);
        groupH.addMemberID(carlitosID);
        groupH.addMemberID(carinaID);
        groupI = new Group(groupIID, description, creationDate, carlaID, groupILedgerID);
        groupI.addMemberID(carlosID);
        groupI.addMemberID(carlitosID);
        groupI.addMemberID(catiaID);
        groupJ = new Group(groupJID, description, creationDate, carlaID, groupJLedgerID);
        groupJ.addMemberID(carlosID);
        groupJ.addMemberID(carlitosID);
        groupJ.addMemberID(catiaID);
    }

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void constructorHappyCaseTest() {
        // Arrange
        // Act
        // Assert
        assertTrue(service instanceof IsFamilyService);
    }

    /**
     * Test for getMembers
     * Happy case
     */
    @Test
    @DisplayName("Test for getMembers - Happy case")
    void getMembersHappyCaseTest() {
        // Arrange

        String expected = "[Person{siblingsIDs=Persons{persons=[]}, accountsIDs=AccountsIDs{AccountsIDs=[]}, id=PersonID{personID=4002@switch.pt}, categories=Categories{categories=[]}, name=Name{name='carla'}, address=Address{address='Rua Santa Catarina 35'}, birthPlace=Birthplace{birthplace='Porto'}, birthDate=Birthdate{birthdate='1989-12-18'}, motherID=null, fatherID=null, ledgerID=LedgerID{ledgerID=4002}}, Person{siblingsIDs=Persons{persons=[]}, accountsIDs=AccountsIDs{AccountsIDs=[]}, id=PersonID{personID=4001@switch.pt}, categories=Categories{categories=[]}, name=Name{name='carlos'}, address=Address{address='Rua Santa Catarina 35'}, birthPlace=Birthplace{birthplace='Porto'}, birthDate=Birthdate{birthdate='1989-12-18'}, motherID=null, fatherID=null, ledgerID=LedgerID{ledgerID=4001}}, Person{siblingsIDs=Persons{persons=[]}, accountsIDs=AccountsIDs{AccountsIDs=[]}, id=PersonID{personID=4003@switch.pt}, categories=Categories{categories=[]}, name=Name{name='carlitos'}, address=Address{address='Rua Santa Catarina 35'}, birthPlace=Birthplace{birthplace='Porto'}, birthDate=Birthdate{birthdate='1989-12-18'}, motherID=PersonID{personID=4001@switch.pt}, fatherID=PersonID{personID=4002@switch.pt}, ledgerID=LedgerID{ledgerID=4003}}, Person{siblingsIDs=Persons{persons=[]}, accountsIDs=AccountsIDs{AccountsIDs=[]}, id=PersonID{personID=4004@switch.pt}, categories=Categories{categories=[]}, name=Name{name='maria'}, address=Address{address='Rua Santa Catarina 35'}, birthPlace=Birthplace{birthplace='Porto'}, birthDate=Birthdate{birthdate='1989-12-18'}, motherID=PersonID{personID=4001@switch.pt}, fatherID=PersonID{personID=4002@switch.pt}, ledgerID=LedgerID{ledgerID=4004}}]";

        // Act
        String actual = service.getMembers(groupA).toString();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for checkIfGroupContainsBothParents
     * Ensure true with child and both parents
     */
    @Test
    @DisplayName("Test for checkIfGroupContainsBothParents - Ensure true with child and both parents")
    void checkIfGroupContainsBothParentsEnsureTrueWithChildAndBothParentsTest() {
        // Arrange;
        // Act
        boolean result = service.checkIfGroupContainsBothParents(groupA);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for checkIfGroupContainsBothParents
     * Ensure false for group with children with only one parent in group
     */
    @Test
    @DisplayName("Test for checkIfGroupContainsBothParents - Ensure false for group with children with only one parent")
    void checkIfGroupContainsBothParentsOnlyOneParentInGroupTest() {
        // Arrange
        // Act
        boolean result = service.checkIfGroupContainsBothParents(groupB);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for checkIfGroupContainsBothParents
     * Ensure false for group without parents
     */
    @Test
    @DisplayName("Test for checkIfGroupContainsBothParents - Ensure false for a group with only 1 member (the creator)")
    void checkIfGroupContainsBothParentsEmptyGroupTest() {
        // Arrange
        // Act
        boolean result = service.checkIfGroupContainsBothParents(groupC);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for getNumberOfChildren
     * Happy Case
     */
    @Test
    @DisplayName("Test for getNumberOfChildren - Happy Case")
    void getNumberOfChildrenHappyCaseTest() {
        // Arrange
        int expected = 2;

        // Act
        int result = service.getNumberOfChildren(groupA);

        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getNumberOfChildren
     * Test with children with only 1 of the parents
     */
    @Test
    @DisplayName("Test for getNumberOfChildren - Only 1 parent")
    void getNumberOfChildrenOnly1ParentTest() {
        // Arrange
        int expected = 2;

        // Act
        int result = service.getNumberOfChildren(groupB);

        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getNumberOfChildren
     * Test with no children in the group
     */
    @Test
    @DisplayName("Test for getNumberOfChildren - No children")
    void getNumberOfChildrenOnlyNoChildrenTest() {
        // Arrange
        int expected = 0;

        // Act
        int result = service.getNumberOfChildren(groupC);

        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for isFamilyGroup
     * Happy case
     */
    @Test
    @DisplayName("Test for isFamily - HappyCase")
    void isFamilyGroupHappyCaseTest() {
        // Arrange
        // Act
        boolean result = service.isFamily(groupA);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for isFamilyGroup
     * Ensure false with missing mother
     */
    @Test
    @DisplayName("Test for isFamily - Ensure false with missing mother")
    void isFamilyGroupEnsureFalseWithMissingMotherTest() {
        // Arrange
        // Act
        boolean result = service.isFamily(groupD);

        // assert
        assertFalse(result);
    }

    /**
     * Test for isFamilyGroup
     * Ensure false with missing father
     */
    @Test
    @DisplayName("Test for isFamilyGroup - Ensure false with missing father")
    void isFamilyGroupEnsureFalseWithMissingFatherTest() {
        // Arrange
        // Act
        boolean result = service.isFamily(groupE);

        // assert
        assertFalse(result);
    }

    /**
     * Test for isFamilyGroup
     * Ensure false with missing children
     */
    @Test
    @DisplayName("Test for isFamilyGroup - Ensure false with missing children")
    void isFamilyGroupEnsureFalseWithMissingChildrenTest() {
        // Arrange
        // Act
        boolean result = service.isFamily(groupF);

        // assert
        assertFalse(result);
    }

    /**
     * Test for isFamilyGroup
     * Ensure true with half siblings father side
     */
    @Test
    @DisplayName("Test for isFamilyGroup - Ensure true with half siblings father side")
    void isFamilyGroupEnsureTrueWithHalfSiblingsFatherSideTest() {
        // Arrange
        // Act
        boolean result = service.isFamily(groupG);

        // assert
        assertTrue(result);

    }

    /**
     * Test for isFamilyGroup
     * Ensure true with half siblings mother side
     */
    @Test
    @DisplayName("Test for isFamilyGroup - Ensure true with half siblings mother side")
    void isFamilyGroupEnsureTrueWithHalfSiblingsMotherSideTest() {
        // Arrange
        // Act
        boolean result = service.isFamily(groupH);

        // assert
        assertTrue(result);
    }

    /**
     * Test for isFamilyGroup
     * Ensure false for group with unrelated child
     */
    @Test
    @DisplayName("Test for isFamilyGroup - Ensure false for group with unrelated child")
    void isFamilyGroupEnsureFalseWithUnrelatedChildTest() {
        // Arrange
        // Act
        boolean result = service.isFamily(groupI);

        // assert
        assertFalse(result);
    }

    /**
     * Test for isFamilyGroup
     * Ensure false with half siblings
     */
    @Test
    @DisplayName("Test for isFamilyGroup - Ensure false with half siblings")
    void isFamilyGroupEnsureFalseWithHalfSiblingsTest() {
        // Arrange
        // Act
        boolean result = service.isFamily(groupJ);

        // assert
        assertFalse(result);
    }


}