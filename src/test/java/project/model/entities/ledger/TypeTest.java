package project.model.entities.ledger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.Designation;

import static org.junit.jupiter.api.Assertions.*;

public class TypeTest {

    /**
     * Test for Type constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for Type constructor - HappyCase")
    void TypeHappyCaseTest() {
        Type debit = Type.DEBIT;
        assertTrue(debit instanceof Type);
    }

//    /**
//     * Test for Type constructor
//     * Exception with Null
//     */
//    @Test
//    @DisplayName("Test for Type constructor - Null Test")
//    void TypeNullTest() {
//        assertThrows(IllegalArgumentException.class, () -> {
//            Type type =new Type(null);
//        });
//    }

//    /**
//     * Test for Type constructor
//     * Exception with string different than "debit" or "credit"
//     */
//    @Disabled
//    @Test
//    @DisplayName("Test for Type constructor - Different than debit/credit Test")
//    void TypeNotDebitCreditTest() {
//        assertThrows(IllegalArgumentException.class, () -> {
//            Type type = new Type("another type");
//        });
//    }

    /**
     * Test for Type equals
     * Happy case
     */
    @Test
    @DisplayName("Test for Type equals - Ensure true with same instance")
    void equalsHappyCase() {
        Type debit = Type.DEBIT;
        boolean result = debit.equals(debit);
        assertTrue(result);
    }

    /**
     * Test for Type equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for Type equals - Ensure false with null")
    void equalsEnsureFalseWithNullObject() {
        Type debit = Type.DEBIT;
        Type typeNull = null;
        boolean result = debit.equals(typeNull);
        assertFalse(result);
    }

    /**
     * Test for Type equals
     * Ensure false different type objects
     */
    @Test
    @DisplayName("Test for Type equals - Ensure false different type objects")
    void equalsEnsureFalseWithDifferentTypeObjects() {
        Type debit = Type.DEBIT;
        Type credit = Type.CREDIT;
        boolean result = debit.equals(credit);
        assertFalse(result);
    }

    /**
     * Test for Type equals
     * Ensure false different class objects
     */
    @Test
    @DisplayName("Test for Type equals - Ensure false different class objects")
    void equalsEnsureFalseWithDifferentClassObjects() {
        Type debit = Type.DEBIT;
        Designation designation = new Designation("credit");
        boolean result = debit.equals(designation);
        assertFalse(result);
    }

    /**
     * Test for toString
     * Happy Case
     */
    @Test
    @DisplayName("Test for toString - Happy case")
    void toStringHappyCase() {
        Type credit = Type.CREDIT;
        String expected = "Type{type=1}";
        String result = credit.toString();
        assertEquals(expected, result);
    }
}
