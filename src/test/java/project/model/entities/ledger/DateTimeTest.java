package project.model.entities.ledger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.Category;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

class DateTimeTest {

    /**
     * Test for DateTime Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for the constructor - Happy Case")
    void DateTimeConstructorHappyCaseTest() {
        // Arrange
        DateTime newDateTime = new DateTime("2020-01-29 10:00:00");

        // Assert
        assertTrue(newDateTime instanceof DateTime);
    }

    /**
     * Test for setDateTime
     */
    @Test
    @DisplayName("Test for setDateTime- Null input")
    void setDateTime () {
        DateTime newDateTime = new DateTime(null);

        LocalDateTime expected = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        LocalDateTime result = newDateTime.dateTimeValue;

        assertEquals(result, expected);

    }

    /**
     * Test for DateTime Equals
     * Happy Case
     */
    @Test
    @DisplayName("Test for Equals - Happy Case")
    void DateTimeEqualsHappyCaseTest() {
        // Arrange
        DateTime dateTime = new DateTime("2020-01-29 10:00:00");
        DateTime newDateTime = dateTime;

        // Assert
        assertTrue(newDateTime.equals(dateTime));
    }

    /**
     * Test for DateTime Equals
     * Ensure fails with different dates
     */
    @Test
    @DisplayName("Test for Equals - different dates")
    void DateTimeEqualsDifferentStringsTest() {
        // Arrange
        DateTime dateTime = new DateTime("2020-01-29 10:00:00");
        DateTime newDateTime = new DateTime("2019-01-29 10:00:00");

        // Assert
        assertFalse(newDateTime.equals(dateTime));
    }

    /**
     * Test for DateTime Equals
     * Ensure fails with null
     */
    @Test
    @DisplayName("Test for Equals - null")
    void DateTimeEqualsNullTest() {
        // Arrange
        DateTime dateTime = new DateTime("2020-01-29 10:00:00");

        // Assert
        assertFalse(dateTime.equals(null));
    }


    /**
     * Test for DateTime Equals
     * Ensure false with objects from different classes
     */
    @Test
    @DisplayName("Test for Equals - objects from different classes")
    void DateTimeEqualsDifferentClassObjectsComparisonTest() {
        // Arrange
        DateTime newDateTime = new DateTime("2020-01-29 10:00:00");
        Category newCategory = new Category("mercearia");

        // Assert
        assertFalse(newDateTime.equals(newCategory));
    }

    /**
     * Test for DateTime hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void TransactionHashCodeEnsureTrueTest() {
        //Arrange
        DateTime newDateTime = new DateTime("2020-01-29 10:00:00");
        int expectedHash = -418823937;

        // Act
        int actualHash = newDateTime.hashCode();

        // Assert
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for toString
     * Happy Case
     */
    @Test
    @DisplayName("Test for toString - Happy case")
    void toStringHappyCase() {
        DateTime newDateTime = new DateTime("2020-01-29 10:00:00");
        String expected = "DateTime{dateTime='2020-01-29T10:00'}";
        String result = newDateTime.toString();
        assertEquals(expected, result);
    }
}