package project.model.entities.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.account.Account;

import static org.junit.jupiter.api.Assertions.*;

class CategoryTest {

    /**
     * Test for Category Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for the constructor - Happy Case")
    void CategoryConstructorHappyCaseTest() {
        Category health = new Category("medication");
        assertTrue(health instanceof Category);
    }

    /**
     * Test for setDesignation
     * Cannot accept a null designation
     */
    @Test
    @DisplayName("Test for the setDesignation - Ensure exception with null name")
    void setDesignationEnsureExceptionWithNullCategoryTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Category health = new Category(null);
        });
    }

    /**
     * Test for Category toString
     * Happy Case
     */
    @Test
    @DisplayName("Test for toString - Happy Case")
    void CategoryToStringHappyCaseTest() {
        //Arrange
        Category newCategory = new Category("Comida");
        String expected = "Category{designation=Designation{designation='Comida'}}";

        //Act
        String result = newCategory.toString();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for Category equals
     * Ensure true with same instance
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same instance")
    void CategoryEqualsHappyCaseTest() {
        Category mainA = new Category("overheads");
        boolean result = mainA.equals(mainA);
        assertTrue(result);
    }

    /**
     * Test for Category equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null object")
    void CategoryEqualsEnsureFalseWithNullTest() {
        Category mainA = new Category("overheads");
        Category mainB = null;
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for Category equals
     * Ensure false with different class object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with different class object")
    void CategoryEqualsEnsureFalseWithDifferentClassObjectTest() {
        Category mainA = new Category("overheads");
        AccountID mainBID = new AccountID("1");
        Account mainB = new Account(mainBID, "wallet", "coins");
        boolean result = mainA.equals(mainB);
        assertFalse(result);
    }

    /**
     * Test for Category equals
     * Ensure true with same attributes
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same attributes")
    void CategoryEqualsEnsureTrueWithSameAttributesTest() {
        Category mainA = new Category("overheads");
        Category mainB = new Category("overheads");
        boolean result = mainA.equals(mainB);
        assertTrue(result);
    }

    /**
     * Test for Category hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void CategoryHashCodeEnsureTrueTest() {
        Category mainA = new Category("overheads");
        int expectedHash = -759327107;
        int actualHash = mainA.hashCode();
        assertEquals(expectedHash, actualHash);
    }
}