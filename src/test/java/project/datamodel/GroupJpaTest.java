package project.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.CreationDate;
import project.model.entities.group.GroupID;
import project.model.entities.shared.Description;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GroupJpaTest {

    /**
     * Constructor test
     * noArgsConstructor
     */
    @Test
    @DisplayName("Test for AccountsDTO constructor - NoArgsConstructor")
    void noArgsConstructorTest() {
        //Arrange
        GroupJpa groupJpa = new GroupJpa();
        //Act
        //Assert
        assertTrue(groupJpa instanceof GroupJpa);
    }

    /**
     * Constructor test
     * Strings Constructor
     */
    @Test
    @DisplayName("Test for AccountsDTO constructor - Strings Constructor")
    void constructorWithStringsTest() {
        //Arrange
        //String id, String description, String creationDate, String creatorID, String ledgerID) {
        GroupJpa groupJpa = new GroupJpa("1", "Family", "2020-04-20", "111@switch.pt", "4");
        //Act
        //Assert
        assertTrue(groupJpa instanceof GroupJpa);
    }

    /**
     * Constructor test
     * VOs Constructor
     */
    @Test
    @DisplayName("Test for AccountsDTO constructor - VOs Constructor")
    void constructorWithVOsTest() {
        //Arrange
        GroupID groupID = new GroupID("1");
        Description description = new Description("Family");
        CreationDate creationDate = new CreationDate("2020-04-20");
        PersonID creatorID = new PersonID("111@switch.pt");
        LedgerID ledgerID = new LedgerID("4");

        GroupJpa groupJpa = new GroupJpa(groupID, description, creationDate, creatorID, ledgerID);
        //Act
        //Assert
        assertTrue(groupJpa instanceof GroupJpa);
    }

    /**
     * getId test
     * VOs Constructor
     */
    @Test
    @DisplayName("Test for AccountsDTO constructor - VOs Constructor")
    void getIdTest() {
        //Arrange
        GroupID groupID = new GroupID("1");
        Description description = new Description("Family");
        CreationDate creationDate = new CreationDate("2020-04-20");
        PersonID creatorID = new PersonID("111@switch.pt");
        LedgerID ledgerID = new LedgerID("4");
        GroupJpa groupJpa = new GroupJpa(groupID, description, creationDate, creatorID, ledgerID);

        //Act
        GroupID result = groupJpa.getId();
        //Assert
        assertEquals(groupID, result);
    }
}