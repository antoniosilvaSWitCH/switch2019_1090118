package project.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.AccountID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountJpaTest {

    /**
     * Constructor test
     * noArgsConstructor
     */
    @Test
    @DisplayName("Test for AccountJPA constructor - NoArgsConstructor")
    void noArgsConstructorTest() {
        //Arrange
        AccountJpa accountJpa = new AccountJpa();
        //Act
        //Assert
        assertTrue(accountJpa instanceof AccountJpa);
    }

    /**
     * Constructor test
     * Strings Constructor
     */
    @Test
    @DisplayName("Test for AccountJPA constructor - Strings Constructor")
    void constructorWithStringsTest() {
        //Arrange
        //String id, String description, String creationDate, String creatorID, String accountJpa) {
        AccountJpa accountJpa = new AccountJpa("5243", "denominationblabla", "descriptionblabla");
        //Act
        //Assert
        assertTrue(accountJpa instanceof AccountJpa);
    }

    /**
     * Test for AccountJpa constructor - with accountID
     */
    @Test
    @DisplayName("Test for AccountJpa constructor - AccountJpa accountID")
    void accountJpaSecondConstructor() {

        //arrange
        AccountID id = new AccountID("23");
        AccountJpa accountJpa = new AccountJpa(id, "denominationblabla", "descriptionblabla");

        //assert
        assertTrue(accountJpa instanceof AccountJpa);

    }


    /**
     * Test for AccountJpa get method
     */
    @Test
    @DisplayName("Test for getAccountJpa ")
    void accountJpaGetMethod() {

        //arrange
        AccountID id = new AccountID("1311");

        AccountJpa accountJpa = new AccountJpa(id, "denominationblabla", "descriptionblabla");

        //act
        String expected = "1311";
        String result = accountJpa.getId().toStringDTO();

        //assert
        assertEquals(expected, result);

    }

    /**
     * Test for AccountJpa set method
     */
    @Test
    @DisplayName("Test for setAccountJpa")
    void accountJpaSetMethod() {

        //arrange
        AccountID accountID = new AccountID("1311");
        AccountID expectedAccountID = new AccountID("3");

        AccountJpa accountJpa = new AccountJpa(accountID, "denominationblabla", "descriptionblabla");

        accountJpa.setId(expectedAccountID);

        //act
        AccountID result = accountJpa.getId();

        //assert
        assertEquals(expectedAccountID, result);
    }

    /**
     * Test for AccountJpa toString method
     */
    @Test
    @DisplayName("Test for toString")
    void toStringMethod() {

        //arrange
        AccountID accountID = new AccountID("1311");
        AccountJpa accountJpa = new AccountJpa(accountID, "denominationblabla", "descriptionblabla");

        String expected = "AccountJpa{id=AccountID{accountID=1311}, denomination='denominationblabla', description='descriptionblabla'}";

        //act
        String result = accountJpa.toString();

        //assert
        assertEquals(expected, result);
    }


}