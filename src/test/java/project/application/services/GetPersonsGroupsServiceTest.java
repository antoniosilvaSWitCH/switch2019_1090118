package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GetPersonsGroupsResponseDTO;
import project.dto.GroupDTOMinimal;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSAddMemberToGroupService;
import project.frameworkddd.IUSGetPersonsGroupsService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class GetPersonsGroupsServiceTest {

    @Autowired
    IUSGetPersonsGroupsService service;

    @Autowired
    IUSAddMemberToGroupService otherService;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    GroupRepository groupRepository;

    /**
     * Test for getPersonsGroups Service Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Get Persons Groups Happy Case - Unit Test")
    void getPersonsGroupsHappyCaseTest() {
        // Arrange
        String joaoIDString = "21001@switch.pt";
        PersonID joaoID = new PersonID(joaoIDString);
        GroupID group1ID = new GroupID("21011");
        LedgerID ledgerGroup1ID = new LedgerID("21031");

        Group group1 = new Group(group1ID, "Switchadas", "1987-04-17", joaoID, ledgerGroup1ID);

        Set<Group> expectedGroups = new HashSet<>();
        expectedGroups.add(group1);

        GroupDTOMinimal groupDTOMinimal = new GroupDTOMinimal("21011", "Switchadas", "1987-04-17");

        List<GroupDTOMinimal> expectedGroupsDTOs = new ArrayList<>();
        expectedGroupsDTOs.add(groupDTOMinimal);

        GetPersonsGroupsResponseDTO expected = new GetPersonsGroupsResponseDTO(expectedGroupsDTOs);

        Person mockedPerson = Mockito.mock(Person.class);
        Group mockedGroup1 = Mockito.mock(Group.class);

        Mockito.when(personRepository.findById(joaoID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepository.findAll()).thenReturn(expectedGroups);
        Mockito.when(mockedGroup1.hasMemberID(joaoID)).thenReturn(true);

        // Act
        GetPersonsGroupsResponseDTO actual = service.getPersonsGroups(joaoIDString);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getPersonsGroups Service Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Get Persons Groups Happy Case Empty Set - Unit Test")
    void getPersonsGroupsHappyCaseTestEmptySet() {
        // Arrange
        String joaoIDString = "21001@switch.pt";
        PersonID joaoID = new PersonID(joaoIDString);

        Set<Group> expectedGroups = new HashSet<>();

        List<GroupDTOMinimal> expectedGroupsDTOs = new ArrayList<>();

        GetPersonsGroupsResponseDTO expected = new GetPersonsGroupsResponseDTO(expectedGroupsDTOs);

        Person mockedPerson = Mockito.mock(Person.class);
        Group mockedGroup1 = Mockito.mock(Group.class);

        Mockito.when(personRepository.findById(joaoID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepository.findAll()).thenReturn(expectedGroups);
        Mockito.when(mockedGroup1.hasMemberID(joaoID)).thenReturn(false);

        // Act
        GetPersonsGroupsResponseDTO actual = service.getPersonsGroups(joaoIDString);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getPersonsGroupsEnsurePersonNotFoundException - Unit Test
     */
    @Test
    @DisplayName("Get Persons Groups Ensure Person Not Found Exception - Unit Test")
    void getPersonsGroupsEnsurePersonNotFoundExceptionTest() {
        // Arrange
        String personIDString = "4001@switch.pt";
        PersonID personID = new PersonID(personIDString);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.empty());

        // Act
        // Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.getPersonsGroups(personIDString);
        });
    }
}
