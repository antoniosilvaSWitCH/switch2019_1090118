package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.*;
import project.frameworkddd.IUSDuplicateAccountService;
import project.frameworkddd.IUSGetCategoriesService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class DuplicateAccountRESTController {

    @Autowired
    private IUSDuplicateAccountService service;

    @PostMapping("/people/{personID}/accounts/{accountID}/duplicate/{newAccountID}")
    public ResponseEntity<Object> duplicateAccount(@PathVariable String personID, @PathVariable String accountID, @PathVariable String newAccountID) {

        DuplicateAccountRequestDTO requestDTO = DuplicateAccountAssembler.mapToRequestDTO(personID, accountID, newAccountID);
        CreateAccountForPersonResponseDTO result = service.duplicateAccount(requestDTO);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }
}
