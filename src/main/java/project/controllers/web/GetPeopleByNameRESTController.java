package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.PeopleResponseDTO;
import project.frameworkddd.IUSGetPeopleByNameService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GetPeopleByNameRESTController {

    @Autowired
    private IUSGetPeopleByNameService service;

    @GetMapping("/people/{personID}/filter")
    public ResponseEntity<Object> getPeopleByName(@PathVariable String personID,
                                                  @RequestParam(name = "name") String name) {

        PeopleResponseDTO responseDTO = service.getPeopleByName(personID, name);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

}
