package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.*;
import project.frameworkddd.IUSCopyCategoryBetweenGroups;
import project.frameworkddd.IUSCreateCategoryForPersonService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CopyCategoryBetweenGroupsRESTController {

    @Autowired
    private IUSCopyCategoryBetweenGroups service;

    @PostMapping("/people/{personID}/groups/copy/category")
    public ResponseEntity<Object> copyCategoryBetweenGroups(@RequestBody CopyCategoryBetweenGroupsInfoDTO info, @PathVariable String personID) {
        CopyCategoryBetweenGroupsRequestDTO requestDTO = CopyCategoryBetweenGroupsAssembler.mapToRequestDTO(personID, info.getGroupIdA(), info.getGroupIdB(), info.getDesignation());
        CreateCategoryForGroupResponseDTO result = service.copyCategoryBetweenGroups(requestDTO);
        Link selfLink = linkTo(methodOn(GetCategoriesRESTController.class).getCategoriesByGroupID(info.getGroupIdB())).withSelfRel();
        result.add(selfLink);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}
