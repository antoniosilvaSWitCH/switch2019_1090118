package project.infrastructure.repositories.jpa;

import org.springframework.data.repository.CrudRepository;
import project.datamodel.AccountJpa;
import project.model.entities.shared.AccountID;

import java.util.List;
import java.util.Optional;

public interface AccountJpaRepository extends CrudRepository<AccountJpa, AccountID> {

    List<AccountJpa> findAll();

    Optional<AccountJpa> findById(AccountID id);
}