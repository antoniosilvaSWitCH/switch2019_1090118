package project.infrastructure.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.datamodel.LedgerDomainDataAssembler;
import project.datamodel.LedgerJpa;
import project.infrastructure.repositories.jpa.LedgerJpaRepository;
import project.model.entities.ledger.Ledger;
import project.model.entities.shared.LedgerID;
import project.model.specifications.repositories.LedgerRepository;

import java.util.Optional;

@Component
public class LedgerRepositoryDB implements LedgerRepository {

    @Autowired
    LedgerJpaRepository ledgerJpaRepository;

    public LedgerRepositoryDB() {
        // This constructor is intentionally empty
    }

    /**
     * Method findByLedgerID
     * gets a ledger with a given ID from the JpaRepository
     *
     * @param ledgerID
     * @return an Optional of Ledger
     */
    public Optional<Ledger> findById(LedgerID ledgerID) {
        Optional<Ledger> result = Optional.empty();

        if (ledgerID != null) {
            Optional<LedgerJpa> opLedgerJpa = ledgerJpaRepository.findById(ledgerID);
            if (opLedgerJpa.isPresent()) {
                LedgerJpa ledgerJpa = opLedgerJpa.get();
                Ledger ledger = LedgerDomainDataAssembler.toDomain(ledgerJpa);
                result = Optional.of(ledger);
            }
        }
        return result;
    }

    /**
     * Method save
     * Save a object of the domain in the database and return a object of the domain
     *
     * @param ledger
     * @return Ledger domain of savedLedgerJpa
     */
    public Ledger save(Ledger ledger) {

        LedgerJpa ledgerJpa = LedgerDomainDataAssembler.toData(ledger);

        LedgerJpa savedLedgerJpa = ledgerJpaRepository.save(ledgerJpa);

        return LedgerDomainDataAssembler.toDomain(savedLedgerJpa);
    }
}