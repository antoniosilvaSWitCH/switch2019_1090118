package project.exceptions;

import static project.exceptions.Messages.INVALIDFIELD;

public class InvalidFieldException extends RuntimeException {
    static final long serialVersionUID = -512383099227709097L;

    /**
     * Exception for when invalid field
     * Uses exceptions.Messages for exception message
     */
    public InvalidFieldException() {
        super(INVALIDFIELD);
    }

    /**
     * Exception for when invalid field
     * Uses provided message for exception message
     */
    public InvalidFieldException(String message) {
        super(message);
    }

}
