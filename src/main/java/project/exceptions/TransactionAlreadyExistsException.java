package project.exceptions;

import static project.exceptions.Messages.TRANSACTIONALREADYEXISTS;

public class TransactionAlreadyExistsException extends RuntimeException {
    static final long serialVersionUID = -5462779037263066262L;

    /**
     * Exception for when transaction already exists
     * Uses exceptions.Messages for exception message
     */
    public TransactionAlreadyExistsException() {
        super(TRANSACTIONALREADYEXISTS);
    }

    /**
     * Exception for when transaction already exists
     * Uses provided message for exception message
     */
    public TransactionAlreadyExistsException(String message) {
        super(message);
    }
}
