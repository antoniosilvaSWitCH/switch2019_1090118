package project.exceptions;

import static project.exceptions.Messages.ACCOUNTALREADYEXISTS;

public class AccountAlreadyExistsException extends RuntimeException {
    static final long serialVersionUID = -302739812478926314L;

    /**
     * Exception for when account already exists
     * Uses exceptions.Messages for exception message
     */
    public AccountAlreadyExistsException() {
        super(ACCOUNTALREADYEXISTS);
    }

    /**
     * Exception for when account already exists
     * Uses provided message exception message
     */
    public AccountAlreadyExistsException(String message) {
        super(message);
    }
}
