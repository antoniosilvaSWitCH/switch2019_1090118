package project.exceptions;

import static project.exceptions.Messages.PERSONNOTFOUND;

public class PersonNotFoundException extends RuntimeException {
    static final long serialVersionUID = 9022205128626695976L;

    /**
     * Exception for when person not found
     * Uses exceptions.Messages for exception message
     */
    public PersonNotFoundException() {
        super(PERSONNOTFOUND);
    }

    /**
     * Exception for when person not found
     * Uses provided message for exception message
     */
    public PersonNotFoundException(String message) {
        super(message);
    }

}
