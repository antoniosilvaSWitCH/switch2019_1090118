package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CategoriesAssembler;
import project.dto.CategoriesDTO;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSGetCategoriesService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Service
public class GetCategoriesService implements IUSGetCategoriesService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private PersonRepository personRepository;

    /**
     * Method getCategoriesByGroupID
     * get all categories of a group
     *
     * @param groupID ID of Group
     * @return CategoriesDTO with a Set<String>
     */
    public CategoriesDTO getCategoriesByGroupID(String groupID) {
        GroupID newGroupID = new GroupID(groupID);

        Optional<Group> optGroup = this.groupRepository.findById(newGroupID);

        Group group;
        if (!optGroup.isPresent()){
            throw new GroupNotFoundException();
        } else {
            group = optGroup.get();

        }

        return CategoriesAssembler.mapToDTO(group.getCategories().getCategoriesValue());
    }

    /**
     * Method getCategoriesByPersonID
     * get all categories of a person
     *
     * @param personID ID of Person
     * @return CategoriesDTO with a Set<String>
     */
    public CategoriesDTO getCategoriesByPersonID(String personID){
        PersonID newPersonID = new PersonID(personID);

        Optional<Person> optPerson = this.personRepository.findById(newPersonID);

        Person person;
        if (!optPerson.isPresent()){
            throw new PersonNotFoundException();
        } else {
            person = optPerson.get();

        }

        return CategoriesAssembler.mapToDTO(person.getCategories().getCategoriesValue());
    }

}