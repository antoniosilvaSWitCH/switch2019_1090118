package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CopyCategoryBetweenGroupsRequestDTO;
import project.dto.CreateCategoryForGroupAssembler;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.exceptions.*;
import project.frameworkddd.IUSCopyCategoryBetweenGroups;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.Category;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Service
public class CopyCategoryBetweenGroupsService implements IUSCopyCategoryBetweenGroups {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    PersonRepository personRepository;

    public CopyCategoryBetweenGroupsService(PersonRepository personRepository, GroupRepository groupRepository) {
        this.personRepository = personRepository;
        this.groupRepository = groupRepository;
    }

    private Person getPersonById(String personID) {
        Optional<Person> personOpt = personRepository.findById(new PersonID(personID));

        if (!personOpt.isPresent()) {
            throw new PersonNotFoundException();
        } else {
            return personOpt.get();
        }

    }

    private Group getGroupById(String groupId) {
        Optional<Group> groupOpt = groupRepository.findById(new GroupID(groupId));

        if (!groupOpt.isPresent()) {
            throw new GroupNotFoundException();
        } else {
            return groupOpt.get();
        }

    }

    private void checkGroupHasManager(Group group, Person person) {
        if (!group.hasManagerID(person.getID())) {
            throw new PersonIsNotManagerOfTheGroupException();
        }
    }

    private void checkGroupHasCategory(Group group, String category) {
        if (!group.getCategories().hasCategory(new Category(category))) {
            throw new CategoryNotFoundException();
        }
    }

    public CreateCategoryForGroupResponseDTO copyCategoryBetweenGroups(CopyCategoryBetweenGroupsRequestDTO requestDTO) {
        Person person = getPersonById(requestDTO.getPersonId());
        Group groupA = getGroupById(requestDTO.getGroupIdA());
        Group groupB = getGroupById(requestDTO.getGroupIdB());
        String designation = requestDTO.getDesignation();

        checkGroupHasManager(groupA, person);
        checkGroupHasManager(groupB, person);
        checkGroupHasCategory(groupA, designation);

        groupB.addCategory(designation);

        Group savedGroup = groupRepository.save(groupB);

        return CreateCategoryForGroupAssembler.mapToResponseDTO(
                savedGroup.getID().toStringDTO(),
                savedGroup.getDescription().getDescriptionValue(),
                designation);

    }

}
