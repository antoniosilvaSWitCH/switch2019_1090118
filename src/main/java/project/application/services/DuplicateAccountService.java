package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CreateAccountForPersonAssembler;
import project.dto.CreateAccountForPersonResponseDTO;
import project.dto.DuplicateAccountAssembler;
import project.dto.DuplicateAccountRequestDTO;
import project.exceptions.*;
import project.frameworkddd.IUSDuplicateAccountService;
import project.model.entities.account.Account;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Transaction;
import project.model.entities.person.Person;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;
import java.util.Set;

@Service
public class DuplicateAccountService implements IUSDuplicateAccountService {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    public DuplicateAccountService(PersonRepository personRepository, AccountRepository accountRepository) {
        this.personRepository = personRepository;
        this.accountRepository = accountRepository;
    }

    private Person getPersonById(String personID) {
        Optional<Person> personOpt = personRepository.findById(new PersonID(personID));

        if (!personOpt.isPresent()) {
            throw new PersonNotFoundException();
        } else {
            return personOpt.get();
        }

    }

    private Ledger getPersonsLedger(Person person) {
        Optional<Ledger> ledgerOpt = ledgerRepository.findById(person.getLedgerID());

        if (!ledgerOpt.isPresent()) {
            throw new LedgerNotFoundException();
        } else {
            return ledgerOpt.get();
        }

    }

    private Account getAccountById(String accountID) {
        Optional<Account> accountOpt = accountRepository.findById(new AccountID(accountID));

        if (!accountOpt.isPresent()) {
            throw new AccountNotFoundException();
        } else {
            return accountOpt.get();
        }

    }

    private boolean checkPersonHasAccount(Person person, String accountID) {
        if (!person.hasAccountID(new AccountID(accountID))) {
            throw new AccountConflictException();
        } else {
            return true;
        }

    }

    private boolean checkAccountDoesNotExist(String accountID) {
        Optional<Account> accountOpt = accountRepository.findById(new AccountID(accountID));

        if (accountOpt.isPresent()) {
            throw new AccountAlreadyExistsException();
        } else {
            return true;
        }
    }

    private void duplicateTransaction(Ledger ledger, Transaction transaction, AccountID accountID, AccountID newAccountID) {
        AccountID accountIDDebit;
        AccountID accountIDCredit;

        if (transaction.getDebitAccountID().equals(accountID)) {
            accountIDDebit = newAccountID;
            accountIDCredit = transaction.getCreditAccountID();
        } else {
            accountIDDebit = transaction.getDebitAccountID();
            accountIDCredit = newAccountID;
        }

        String dateTimeMod = transaction.getDateTime().toStringDTO().replace("T", " ");
        ledger.addTransaction(transaction.getAmount().toStringDTO(), transaction.getType(), dateTimeMod, transaction.getDescription().getDescriptionValue(), transaction.getCategory(), accountIDDebit, accountIDCredit);
        ledgerRepository.save(ledger);

    }

    public CreateAccountForPersonResponseDTO duplicateAccount(DuplicateAccountRequestDTO requestDTO) {

        // Checks
        Person person = getPersonById(requestDTO.getPersonID());
        Account account = getAccountById(requestDTO.getAccountID());
        checkPersonHasAccount(person, requestDTO.getAccountID());
        checkAccountDoesNotExist(requestDTO.getNewAccountID());

        // Create new account, edit accountRepo and personRepo
        AccountID accountID = new AccountID(requestDTO.getAccountID());
        AccountID newAccountID = new AccountID(requestDTO.getNewAccountID());
        person.addAccount(newAccountID);
        Account newAccount = new Account(newAccountID, account.getDenomination().getDenominationValue(), account.getDescription().getDescriptionValue());
        accountRepository.save(newAccount);
        personRepository.save(person);

        // Get person's transactions with accountID
        Ledger ledger = getPersonsLedger(person);
        Set<Transaction> transactions = ledger.getTransactionsOfAccount(accountID);

        // Duplicate transactions in ledger, save ledger in repo
        for (Transaction transaction : transactions) {
            duplicateTransaction(ledger, transaction, accountID, newAccountID);
        }

        return CreateAccountForPersonAssembler.mapToResponseDTO(person.getID().toStringDTO(),
                person.getName().toStringDTO(),
                newAccount.getID().toStringDTO(),
                newAccount.getDenomination().toStringDTO());
    }

}
