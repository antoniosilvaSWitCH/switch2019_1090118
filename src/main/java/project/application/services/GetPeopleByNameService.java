package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.PeopleAssembler;
import project.dto.PeopleResponseDTO;
import project.dto.TransactionsAssembler;
import project.dto.TransactionsResponseDTO;
import project.exceptions.*;
import project.frameworkddd.IUSGetPeopleByNameService;
import project.frameworkddd.IUSGetTransactionsService;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Transaction;
import project.model.entities.person.Person;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.Period;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class GetPeopleByNameService implements IUSGetPeopleByNameService {

    @Autowired
    private PersonRepository personRepository;

    private Person getPersonById(String personID) {
        Optional<Person> personOpt = personRepository.findById(new PersonID(personID));

        if (!personOpt.isPresent()) {
            throw new PersonNotFoundException();
        } else {
            return personOpt.get();
        }

    }

    @Override
    public PeopleResponseDTO getPeopleByName(String personID, String name) {

        getPersonById(personID);

        Set<Person> people = personRepository.findByName(name);

        return PeopleAssembler.mapToResponseDTO(people);

    }

}
