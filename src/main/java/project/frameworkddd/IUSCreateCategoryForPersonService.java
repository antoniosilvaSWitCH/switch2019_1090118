package project.frameworkddd;

import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;

public interface IUSCreateCategoryForPersonService {

    CreateCategoryForGroupResponseDTO createCategoryForGroup(CreateCategoryForGroupRequestDTO requestDTO);

}
