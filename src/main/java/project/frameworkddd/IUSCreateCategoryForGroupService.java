package project.frameworkddd;

import project.dto.CreateCategoryForPersonRequestDTO;
import project.dto.CreateCategoryForPersonResponseDTO;

public interface IUSCreateCategoryForGroupService {

    CreateCategoryForPersonResponseDTO createCategoryForPerson(CreateCategoryForPersonRequestDTO requestDTO);

}
