package project.frameworkddd;

import project.dto.CopyCategoryBetweenGroupsRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;

public interface IUSCopyCategoryBetweenGroups {

    CreateCategoryForGroupResponseDTO copyCategoryBetweenGroups(CopyCategoryBetweenGroupsRequestDTO requestDTO);

}
