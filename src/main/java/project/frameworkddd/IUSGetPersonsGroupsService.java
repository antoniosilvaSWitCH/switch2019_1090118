package project.frameworkddd;

import project.dto.GetPersonsGroupsResponseDTO;

public interface IUSGetPersonsGroupsService {
    GetPersonsGroupsResponseDTO getPersonsGroups(String personID);

    GetPersonsGroupsResponseDTO getPersonsGroupsBeforeDate(String personID, String date);

}
