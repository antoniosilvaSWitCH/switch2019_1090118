package project.frameworkddd;

import project.dto.PeopleResponseDTO;

public interface IUSGetPeopleByNameService {
    PeopleResponseDTO getPeopleByName(String personID, String name);
}
