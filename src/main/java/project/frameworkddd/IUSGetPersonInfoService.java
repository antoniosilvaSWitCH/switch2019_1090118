package project.frameworkddd;

import project.dto.GetPersonInfoResponseDTO;

public interface IUSGetPersonInfoService {

    GetPersonInfoResponseDTO getPersonInfo(String personID);
}
