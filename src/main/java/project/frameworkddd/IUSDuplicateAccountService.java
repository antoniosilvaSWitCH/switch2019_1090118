package project.frameworkddd;

import project.dto.CreateAccountForPersonResponseDTO;
import project.dto.DuplicateAccountRequestDTO;

public interface IUSDuplicateAccountService {
    CreateAccountForPersonResponseDTO duplicateAccount(DuplicateAccountRequestDTO requestDTO);
}
