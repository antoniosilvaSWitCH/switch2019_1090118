package project.model.specifications.repositories;

import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;

import java.util.Optional;
import java.util.Set;

/**
 * Interface PersonRepository
 */
public interface PersonRepository {

    Person save(final Person object);

    Optional<Person> findById(PersonID personID);

    Set<Person> findByName(String name);

}
