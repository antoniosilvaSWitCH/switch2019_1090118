package project.model.entities.group;

import lombok.NoArgsConstructor;
import project.frameworkddd.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Embeddable
public class GroupID implements ValueObject, Serializable {

    private Long id;

    /**
     * Constructor for GroupID
     *
     * @param groupID unique identifier number of GroupID
     */
    public GroupID(String groupID) {
        this.id = Long.parseLong(groupID);
    }

    /**
     * Compares internal attribute id of two GroupID objects
     * Condition to be verified in equals() override
     *
     * @param other
     * @return
     */
    public boolean sameValueAs(GroupID other) {
        return other != null && this.id.equals(other.id);
    }

    /**
     * Get attribute id
     *
     * @return id attribute
     */
    public long getId() {
        return id;
    }

    /**
     * Method toStringDTO
     *
     * @return id attribute converted to String
     */
    public String toStringDTO() {
        return id.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GroupID)) {
            return false;
        }
        GroupID other = (GroupID) o;
        return sameValueAs(other);
    }

    @Override
    public String toString() {
        return "GroupID{" +
                "groupID=" + id +
                '}';
    }
}
