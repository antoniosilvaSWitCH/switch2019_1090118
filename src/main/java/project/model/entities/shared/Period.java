package project.model.entities.shared;

import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class Period implements ValueObject {
    private static final String DATETIMEFORMAT = "yyyy-MM-dd HH:mm:ss";
    LocalDateTime initialDateTime;
    LocalDateTime finalDateTime;

    /**
     * Constructor for Class Period
     *
     * @param initialDateTime start of period
     * @param finalDateTime   end of period
     */
    public Period(String initialDateTime, String finalDateTime) {
        setInitialAndFinalDateTime(initialDateTime, finalDateTime);
    }

    /**
     * Create attributes initialDateTime and finalDateTime
     * Thows exceptions for invalid dateTime formats (initial or final), if initialDateTime is after finalDateTime
     *
     * @param initialDateTime start of period
     * @param finalDateTime   end of period
     */
    private void setInitialAndFinalDateTime(String initialDateTime, String finalDateTime) {
        if (isDateTimeFormatValid(initialDateTime) && isDateTimeFormatValid(finalDateTime)) {
            LocalDateTime initialDateTimeParsed = LocalDateTime.parse(initialDateTime, DateTimeFormatter.ofPattern(DATETIMEFORMAT));
            LocalDateTime finalDateTimeParsed = LocalDateTime.parse(finalDateTime, DateTimeFormatter.ofPattern(DATETIMEFORMAT));
            if (initialDateTimeParsed.isAfter(finalDateTimeParsed)) {
                throw new InvalidFieldException("Input 'initialDateTime' cannot be after 'finalDateTime'!");
            } else if (initialDateTimeParsed.equals(finalDateTimeParsed)) {
                throw new InvalidFieldException("Final dateTime can't be equal to initial dateTime.");
            } else {
                this.initialDateTime = initialDateTimeParsed;
                this.finalDateTime = finalDateTimeParsed;
            }
        } else {
            throw new InvalidFieldException("Input 'initialDateTime' or 'finalDateTime' is invalid!");
        }
    }

    /**
     * Format validator for dateTime
     *
     * @param dateTime dateTime string
     * @return true/false
     */
    private boolean isDateTimeFormatValid(String dateTime) {
        try {
            LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern(DATETIMEFORMAT));
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    /**
     * Check if date is within period
     *
     * @param newDateTime DateTime to check
     * @return true/false
     */
    public boolean isDateTimeWithinPeriod(LocalDateTime newDateTime) {
        return (newDateTime.isEqual(initialDateTime) || newDateTime.isAfter(initialDateTime)) && (newDateTime.isEqual(finalDateTime) || newDateTime.isBefore(finalDateTime));
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Period that = (Period) o;
        return initialDateTime.equals(that.initialDateTime) && finalDateTime.equals(that.finalDateTime);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(initialDateTime, finalDateTime);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Period{" +
                "initialDateTime=" + initialDateTime +
                ", finalDateTime=" + finalDateTime +
                '}';
    }
}
