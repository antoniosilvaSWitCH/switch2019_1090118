package project.model.entities.shared;

import lombok.NoArgsConstructor;
import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Embeddable
public class Designation implements ValueObject, Serializable {
    private String designationValue;

    /**
     * Constructor for Class Designation
     *
     * @param designation Designation of the category
     */
    public Designation(String designation) {
        setDesignationValue(designation);
    }

    /**
     * getDesignationValue method
     *
     * @return designationValue
     */
    public String getDesignationValue() {
        return designationValue;
    }

    /**
     * Create attribute designation
     * Throw exception if the string is null, empty or composed by just spaces
     *
     * @param designationValue
     */
    private void setDesignationValue(String designationValue) {
        if (designationValue != null && !designationValue.isEmpty() && !designationValue.trim().isEmpty()) {
            this.designationValue = designationValue;
        } else {
            throw new InvalidFieldException("Input 'designation' is invalid!");
        }
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Designation newDesignation = (Designation) o;
        return Objects.equals(this.designationValue, newDesignation.designationValue);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(designationValue);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Designation{" +
                "designation='" + designationValue + '\'' +
                '}';
    }

    /**
     * toStringDTO method
     *
     * @return String of designation
     */
    public String toStringDTO() {
        return designationValue;
    }
}
