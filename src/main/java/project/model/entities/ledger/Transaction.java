package project.model.entities.ledger;

import lombok.Getter;
import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.Description;
import project.model.entities.shared.Period;

import java.io.Serializable;
import java.util.Objects;

@Getter
public class Transaction implements ValueObject, Serializable {
    private Amount amount;
    private DateTime dateTime;
    private Type type;
    private Description description;
    private Category category;
    private AccountID debitAccountID;
    private AccountID creditAccountID;

    /**
     * Constructor for Transaction
     *
     * @param amount          Amount of the Transaction
     * @param type            Type of the Transaction
     * @param dateTime        DateTime of the Transaction
     * @param description     Description of the Transaction
     * @param category        Category of the Transaction
     * @param debitAccountID  ID of the Debit Account of the Transaction
     * @param creditAccountID ID of the Credit Account of the Transaction
     */
    public Transaction(String amount, Type type, String dateTime, String description, Category category,
                       AccountID debitAccountID, AccountID creditAccountID) {
        setAmount(amount);
        setType(type);
        setDateTime(dateTime);
        setDescription(description);
        setCategory(category);
        setDebitAccountID(debitAccountID);
        setCreditAccountID(creditAccountID);
    }

    /**
     * Create attribute dateTime
     * Checks if dateTime is appropriate (not null, not empty, not composed by just spaces, does not contain letters,
     * characters at index 5 & 8 are "-" or "/")
     *
     * @param dateTime Transaction dateTime
     */
    private void setDateTime(String dateTime) {
        this.dateTime = new DateTime(dateTime);
    }

    /**
     * Create attribute type
     *
     * @param type Transaction type
     */
    private void setType(Type type) {
        this.type = type;
    }

    /**
     * Create attribute description
     *
     * @param description Transaction description
     */
    private void setDescription(String description) {
        this.description = new Description(description);
    }

    /**
     * Crete attribute category
     *
     * @param category category of transaction
     */
    private void setCategory(Category category) {
        if (category != null) {
            this.category = category;
        } else {
            throw new InvalidFieldException("Input 'category' is null!");
        }
    }

    /**
     * Create attribute debitAccountID
     *
     * @param debitAccountID Account from where the amount is going to be subtracted
     */
    private void setDebitAccountID(AccountID debitAccountID) {
        if (debitAccountID != null) {
            this.debitAccountID = debitAccountID;
        } else {
            throw new InvalidFieldException("Input 'debitAccountID' is null!");
        }
    }

    /**
     * Create attribute creditAccountID
     *
     * @param creditAccountID Account to where the amount is going to be added
     */
    private void setCreditAccountID(AccountID creditAccountID) {
        if (creditAccountID != null) {
            this.creditAccountID = creditAccountID;
        } else {
            throw new InvalidFieldException("Input 'creditAccountID' is null!");
        }
    }

    /**
     * Check if transaction date is within period
     *
     * @param period Period that defines time bounds
     */
    public boolean isWithinPeriod(Period period) {
        return period.isDateTimeWithinPeriod(this.dateTime.dateTimeValue);
    }

    /**
     * Get factored amount of transaction
     *
     * @return amount of transaction multiplied by sign related with debit/credit
     */
    public double getFactoredAmount() {
        return this.type.typeValue * this.amount.amountValue;
    }

    /**
     * Create attribute transactionAmount
     *
     * @param amount Value to be subtracted and added to debitAccount and creditAccount respectively
     */
    private void setAmount(String amount) {
        this.amount = new Amount(amount);
    }

    /**
     * Check if account is associated with transaction (credit or debit)
     *
     * @param accountID Account associated with transaction
     * @return transactionIsFromAccount True if transaction has account associated
     */
    public boolean hasAccountID(AccountID accountID) {
        return hasCreditAccountID(accountID) || hasDebitAccountID(accountID);
    }

    /**
     * Check if is credit account of transaction
     *
     * @param creditAccountID Credit account associated with transaction
     * @return transactionIsFromAccount True if account is credit of transaction
     */
    public boolean hasCreditAccountID(AccountID creditAccountID) {
        boolean transactionIsFromAccountID = false;

        if (creditAccountID != null) {
            if (this.creditAccountID.equals(creditAccountID)) {
                transactionIsFromAccountID = true;
            }
        } else {
            throw new InvalidFieldException("'creditAccountID' cannot be null.");
        }
        return transactionIsFromAccountID;
    }

    /**
     * @param debitAccountID Debit account associated with transaction
     * @return transactionIsFromAccount True if account is debit of transaction
     */
    public boolean hasDebitAccountID(AccountID debitAccountID) {
        boolean transactionIsFromAccountID = false;

        if (debitAccountID != null) {
            if (this.debitAccountID.equals(debitAccountID)) {
                transactionIsFromAccountID = true;
            }
        } else {
            throw new InvalidFieldException("'debitAccountID' cannot be null.");
        }
        return transactionIsFromAccountID;
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Transaction{" +
                "amount=" + amount +
                ", dateTime=" + dateTime +
                ", type=" + type +
                ", description=" + description +
                ", category=" + category +
                ", debitAccountID=" + debitAccountID +
                ", creditAccountID=" + creditAccountID +
                '}';
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transaction that = (Transaction) o;
        return Objects.equals(amount, that.amount) &&
                Objects.equals(type, that.type) &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(description, that.description) &&
                Objects.equals(category, that.category) &&
                Objects.equals(debitAccountID, that.debitAccountID) &&
                Objects.equals(creditAccountID, that.creditAccountID);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(amount, dateTime, description, category, debitAccountID, creditAccountID);
    }
}
