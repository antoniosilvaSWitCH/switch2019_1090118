package project.model.entities.ledger;

import lombok.Getter;
import project.frameworkddd.ValueObject;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

@Getter
public class DateTime implements ValueObject, Serializable {
    LocalDateTime dateTimeValue;

    public DateTime(String dateTime) {
        setDateTimeValue(dateTime);
    }

    /**
     * Create attribute dateTime
     * Checks if time is appropriate (not null, not empty, not composed by just spaces, does not contain letters,
     * characters at index 5 & 8 are "-")
     *
     * @param dateTimeValue dateTime string
     */
    public void setDateTimeValue(String dateTimeValue) {
        if (dateTimeValue == null) {
            this.dateTimeValue = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        } else {
            this.dateTimeValue = LocalDateTime.parse(dateTimeValue, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

    /**
     * Override of toString.
     */
    @Override
    public String toString() {
        return "DateTime{" +
                "dateTime='" + dateTimeValue + '\'' +
                '}';
    }

    public String toStringDTO() {
        return "" + dateTimeValue;
    }

    /**
     * Override of equals.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DateTime that = (DateTime) o;
        return Objects.equals(dateTimeValue, that.dateTimeValue);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(dateTimeValue);
    }
}