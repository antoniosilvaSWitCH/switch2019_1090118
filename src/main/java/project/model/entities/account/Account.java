package project.model.entities.account;

import project.exceptions.InvalidFieldException;
import project.frameworkddd.Root;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Description;

import java.util.Objects;

public class Account implements Root<AccountID> {

    private final Denomination denomination;
    private final Description description;
    private AccountID id;

    /**
     * Constructor for Class Account
     *
     * @param denomination Name of Account
     * @param description  Description of Account
     */
    public Account(AccountID id, String denomination, String description) {
        setId(id);
        this.denomination = new Denomination(denomination);
        this.description = new Description(description);
    }

    /**
     * Set accountID
     * Throws exception if null
     *
     * @param id ID of Account
     */
    private void setId(AccountID id) {
        if (id != null) {
            this.id = id;
        } else {
            throw new InvalidFieldException("Input 'id' cannot be null!");
        }
    }

    /**
     * Get accountID
     *
     * @return accountID  ID of account
     */
    public AccountID getID() {
        return id;
    }

    /**
     * Check if same identity
     *
     * @return true  if equal id objects
     */
    public boolean sameIdentityAs(Account other) {
        return this.id.sameValueAs(other.id);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                "denomination=" + denomination +
                ", description=" + description +
                '}';
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account other = (Account) o;
        return sameIdentityAs(other);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, denomination, description);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    public Denomination getDenomination() {
        return this.denomination;
    }

    public Description getDescription() {
        return this.description;
    }
}
