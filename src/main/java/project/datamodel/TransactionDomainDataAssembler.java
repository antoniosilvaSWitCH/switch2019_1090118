package project.datamodel;

import org.springframework.stereotype.Service;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;

import java.time.format.DateTimeFormatter;

@Service
public class TransactionDomainDataAssembler {

    /**
     * Constructor
     */
    private TransactionDomainDataAssembler() {
        //Constructor is intentionally empty
    }

    /**
     * Method to convert Transaction(Model) to TransactionJpa(Data Model)
     *
     * @param transaction Model transaction
     * @return Data Model TransactionJpa
     */
    public static TransactionJpa toData(LedgerJpa ledgerJpa, Transaction transaction) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateTime = transaction.getDateTime().getDateTimeValue().format(formatter);

        String type;
        if (transaction.getType().equals(Type.DEBIT)) {
            type = "Debit";
        } else type = "Credit";

        return new TransactionJpa(ledgerJpa, transaction.getAmount(), dateTime, type, transaction.getDescription(),
                transaction.getCategory(), transaction.getDebitAccountID(), transaction.getCreditAccountID());
    }

    /**
     * Method to convert TransactionJpa(Data Model) to Transaction(Model)
     *
     * @param transactionJpa Data Model transactionJpa
     * @return Model transaction
     */
    public static Transaction toDomain(TransactionJpa transactionJpa) {

        String amount = String.valueOf(transactionJpa.getAmount().getAmountValue());

        Type type;
        if (transactionJpa.getType().equalsIgnoreCase("Debit")) {
            type = Type.DEBIT;
        } else type = Type.CREDIT;

        return new Transaction(amount, type, transactionJpa.getDateTime(),
                transactionJpa.getDescription().getDescriptionValue(), transactionJpa.getCategory(),
                transactionJpa.getDebitAccountId(),
                transactionJpa.getCreditAccountId());
    }

}
