package project.dto;

import project.model.entities.person.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class PeopleAssembler {

    /**
     * Constructor method left empty because class is static
     */
    PeopleAssembler() {
        //This constructor is intentionally empty
    }

    public static PeopleResponseDTO mapToResponseDTO(Set<Person> people) {
        List<String> personsIDsStrings = new ArrayList<>();
        List<String> namesStrings = new ArrayList<>();

        for (Person member : people) {
            personsIDsStrings.add(member.getID().getPersonEmail());
            namesStrings.add(member.getName().toStringDTO());
        }

        return new PeopleResponseDTO(personsIDsStrings, namesStrings);
    }


}
