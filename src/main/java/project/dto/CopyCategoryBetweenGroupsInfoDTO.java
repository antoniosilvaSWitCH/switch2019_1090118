package project.dto;

import lombok.Data;

@Data
public class CopyCategoryBetweenGroupsInfoDTO {

    private String groupIdA;
    private String groupIdB;
    private String designation;

    public CopyCategoryBetweenGroupsInfoDTO(String groupIdA, String groupIdB, String designation) {
        this.groupIdA = groupIdA;
        this.groupIdB = groupIdB;
        this.designation = designation;
    }

    CopyCategoryBetweenGroupsInfoDTO() {
        //Added because of the dependency injection of the framework spring. Needs a empty constructor.
    }


}
