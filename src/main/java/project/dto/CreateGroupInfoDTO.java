package project.dto;

import java.util.Objects;

public class CreateGroupInfoDTO {

    private String groupID;
    private String description;
    private String creationDate;
    private String groupLedgerID;

    /**
     * Constructor of CreateGroupRequestDTO Class
     *
     * @param groupID       - Identity of the new group to be created as a String
     * @param description   - Description of the new group to be created as a String
     * @param creationDate  - Creation date of the new group to be created as a String
     * @param groupLedgerID - Identity of the new group's ledger to be created upon group's creation
     */
    public CreateGroupInfoDTO(String groupID, String description, String creationDate, String groupLedgerID) {
        setGroupID(groupID);
        setDescription(description);
        setCreationDate(creationDate);
        setGroupLedgerID(groupLedgerID);
    }

    /**
     * getGroupID method
     *
     * @return the identity of the group to be created as a String
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * setGroupID method
     * sets the value of the CreateGroupInfoDTO's groupID attribute to contain the new group's ID as a String
     *
     * @param groupID - Identity of the new group to be created as a String
     */
    private void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * getDescription method
     *
     * @return the description of the new group to be created as a String
     */
    public String getDescription() {
        return description;
    }

    /**
     * setDescription method
     * sets the value of the CreateGroupInfoDTO's description attribute to contain the new group's description as a String
     *
     * @param description - description of the new group to be created as a String
     */
    private void setDescription(String description) {
        this.description = description;
    }

    /**
     * getCreationDate method
     *
     * @return the creation date of the new group to be created as a String
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * setCreationDate method
     * sets the value of the CreateGroupInfoDTO's creationDate attribute to contain the new group's creationDate as a String
     *
     * @param creationDate - creation date of the new group to be created as a String
     */
    private void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * getGroupLedgerID method
     *
     * @return the identity of the ledger of the group to be created as a String
     */
    public String getGroupLedgerID() {
        return groupLedgerID;
    }

    /**
     * setGroupLedgerID method
     * sets the value of the CreateGroupInfoDTO's groupLedgerID attribute to contain the identity of the new group's ledger as a String
     *
     * @param groupLedgerID - identity of the new group's ledger as a String
     */
    private void setGroupLedgerID(String groupLedgerID) {
        this.groupLedgerID = groupLedgerID;
    }

    /**
     * Override of equals method
     *
     * @param o an Object
     * @return True if this Object is equal Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateGroupInfoDTO that = (CreateGroupInfoDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(description, that.description) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(groupLedgerID, that.groupLedgerID);
    }

    /**
     * Override of hashCode method
     *
     * @return an integer with the hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(groupID, description, creationDate, groupLedgerID);
    }
}
