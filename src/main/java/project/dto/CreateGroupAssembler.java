package project.dto;

import project.model.entities.group.Group;
import project.model.entities.shared.PersonID;
import project.model.entities.shared.PersonsIDs;

import java.util.HashSet;
import java.util.Set;

public final class CreateGroupAssembler {

    /**
     * Constructor for CreateGroupAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    CreateGroupAssembler() {
        //Constructor is intentionally empty
    }

    /**
     * mapToDTO method
     * <p>
     * convert a Group instance into a DTO only with the following information inside:
     * members, managers, groupID, ledgerID, description, creationDate
     *
     * @param group object to be converted into DTO
     * @return GroupResponseDTO
     */
    public static GroupDTO mapToDTO(Group group) {
        Set<String> members = convertPersonsIDs(group.getMembersIDs());
        Set<String> managers = convertPersonsIDs(group.getManagersIDs());
        String groupID = group.getID().toStringDTO();
        String groupLedgerID = group.getLedgerID().toStringDTO();
        String description = group.getDescription().getDescriptionValue();
        String creationDate = group.getCreationDate().toStringDTO();

        return new GroupDTO(members, managers, groupID, groupLedgerID, description, creationDate);
    }

    public static CreateGroupResponseDTO mapToResponseDTO(Group group) {
        String groupID = group.getID().toStringDTO();
        String description = group.getDescription().getDescriptionValue();

        return new CreateGroupResponseDTO(groupID, description);
    }

    /**
     * mapToRequestDTO method
     * <p>
     * creates a requestDTO from the input String values to be used in the domain layer
     *
     * @param groupID       - Identity of the new group to be created as a String
     * @param description   - Description of the new group to be created as a String
     * @param creationDate  - Creation date of the new group to be created as a String
     * @param creatorEmail  - Identity of the new group's creator person as a String, who will be member and manager of the group
     * @param groupLedgerID - Identity of the new group's ledger to be created upon group's creation
     * @return an instance of CreateGroupRequestDTO
     */
    public static CreateGroupRequestDTO mapToRequestDTO(String groupID, String description, String creationDate, String creatorEmail, String groupLedgerID) {
        return new CreateGroupRequestDTO(groupID, description, creationDate, creatorEmail, groupLedgerID);
    }

    /**
     * convertPersonsIDs method
     * <p>
     * converts a Set of PersonIDs into a Set of Strings of PersonIDs
     *
     * @param personsIDsVO list of PersonID objects
     * @return Set of personIDs converted to Strings
     */
    private static Set<String> convertPersonsIDs(PersonsIDs personsIDsVO) {
        Set<PersonID> personsIDs = personsIDsVO.getPersonIDs();
        Set<String> personsIDsToString = new HashSet<>();
        for (PersonID personID : personsIDs) {
            personsIDsToString.add(personID.getPersonEmail());
        }
        return personsIDsToString;
    }
}