package project.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.List;

@Getter
@EqualsAndHashCode(callSuper = false)
public class GetPersonsGroupsResponseDTO extends RepresentationModel<GetPersonsGroupsResponseDTO> {

    private List<GroupDTOMinimal> groupsDTOs;

    /**
     * Constructor for GetPersonsGroupsResponseDTO
     *
     * @param groupsDTOs Set of groups as DTOs
     */
    public GetPersonsGroupsResponseDTO(List<GroupDTOMinimal> groupsDTOs) {
        this.groupsDTOs = Collections.unmodifiableList(groupsDTOs);
    }

    /**
     * Method toString
     */
    @Override
    public String toString() {
        return "GetPersonsGroupsResponseDTO{" +
                "groups=" + groupsDTOs +
                '}';
    }
}
