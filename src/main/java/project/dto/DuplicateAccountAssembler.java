package project.dto;

public class DuplicateAccountAssembler {

    DuplicateAccountAssembler() {
        //Intentionally empty
    }

    public static CreateAccountForPersonResponseDTO mapToResponseDTO(String personEmail, String name, String accountID, String denomination) {
        return new CreateAccountForPersonResponseDTO(personEmail, name, accountID, denomination);
    }

    public static DuplicateAccountRequestDTO mapToRequestDTO(String personID, String accountID, String newAccountID) {
        return new DuplicateAccountRequestDTO(personID, accountID, newAccountID);
    }

}
