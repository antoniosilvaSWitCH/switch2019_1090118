package project.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.Set;

@Getter
@EqualsAndHashCode(callSuper = false)
public class CategoriesDTO extends RepresentationModel<CategoriesDTO> {

    private final Set<CategoryDTO> categories;

    /**
     * Constructor for CreateCategoryForGroupDTO
     */
    public CategoriesDTO(Set<CategoryDTO> categories) {
        this.categories = Collections.unmodifiableSet(categories);
    }

    @Override
    public String toString() {
        return "CategoriesDTO{" +
                "categories=" + categories +
                '}';
    }
}
