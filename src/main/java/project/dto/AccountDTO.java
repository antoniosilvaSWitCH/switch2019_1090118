package project.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@EqualsAndHashCode(callSuper = false)
public class AccountDTO extends RepresentationModel<AccountDTO> {

    private final String denomination;
    private final String description;
    private final String accountID;

    public AccountDTO(String denomination, String description, String accountID) {
        this.denomination = denomination;
        this.description = description;
        this.accountID = accountID;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "denomination='" + denomination + '\'' +
                ", description='" + description + '\'' +
                ", accountID='" + accountID + '\'' +
                '}';
    }
}
