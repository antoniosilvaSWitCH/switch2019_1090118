package project.dto;

import project.model.entities.Categories;
import project.model.entities.group.Group;
import project.model.entities.shared.Category;

import java.util.HashSet;
import java.util.Set;

public final class CopyCategoryBetweenGroupsAssembler {

    CopyCategoryBetweenGroupsAssembler() {
        //Intentionally empty
    }

    public static CopyCategoryBetweenGroupsRequestDTO mapToRequestDTO(String personID, String groupIdA, String groupIdB, String designation) {
        return new CopyCategoryBetweenGroupsRequestDTO(personID, groupIdA, groupIdB, designation);
    }

}
