package project.dto;

import lombok.Data;

@Data
public class CopyCategoryBetweenGroupsRequestDTO {

    private String personId;
    private String groupIdA;
    private String groupIdB;
    private String designation;

    public CopyCategoryBetweenGroupsRequestDTO(String personId, String groupIdA, String groupIdB, String designation) {
        this.personId = personId;
        this.groupIdA = groupIdA;
        this.groupIdB = groupIdB;
        this.designation = designation;
    }

}
