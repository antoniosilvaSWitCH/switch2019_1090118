package project.dto;

import java.util.Objects;

public class CreateAccountForPersonInfoDTO {
    private final String accountID;
    private final String denomination;
    private final String description;

    /**
     * Constructor for CreateAccountForPersonInfoDTO
     *
     * @param accountID    String of accountID to be created
     * @param denomination String of denomination to be created
     * @param description  String description to be created
     */
    public CreateAccountForPersonInfoDTO(String accountID, String denomination, String description) {
        this.accountID = accountID;
        this.denomination = denomination;
        this.description = description;
    }

    /**
     * get method to the accountID
     *
     * @return accountID
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * get method to the account denomination
     *
     * @return denomination
     */
    public String getDenomination() {
        return denomination;
    }

    /**
     * get method to the account description
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateAccountForPersonInfoDTO that = (CreateAccountForPersonInfoDTO) o;
        return Objects.equals(accountID, that.accountID) &&
                Objects.equals(denomination, that.denomination) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountID, denomination, description);
    }


}
