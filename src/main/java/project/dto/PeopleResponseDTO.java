package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.List;

public class PeopleResponseDTO extends RepresentationModel<PeopleResponseDTO> {

    private List<String> membersIDs;
    private List<String> names;

    public PeopleResponseDTO(List<String> membersIDs, List<String> names) {
        setMembersIDs(membersIDs);
        setNames(names);
    }

    /**
     * Get method
     *
     * @return Group list of members IDs
     */
    public List<String> getMembersIDs() {
        return Collections.unmodifiableList(membersIDs);
    }

    /**
     * List method
     *
     * @param membersIDs Group list of members IDs in string format
     */
    private void setMembersIDs(List<String> membersIDs) {
        this.membersIDs = Collections.unmodifiableList(membersIDs);
    }

    public List<String> getNames() {
        return Collections.unmodifiableList(names);
    }

    public void setNames(List<String> names) {
        this.names = Collections.unmodifiableList(names);
    }

}
