package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CreateCategoryForPersonResponseDTO extends RepresentationModel<CreateCategoryForPersonResponseDTO> {

    private String personID;
    private String designation;

    /**
     * Constructor for CreateCategoryResponseDTO
     *
     * @param personID
     * @param designation
     */
    public CreateCategoryForPersonResponseDTO(String personID, String designation) {
        setPersonID(personID);
        setDesignation(designation);
    }

    /**
     * Method getPersonID
     *
     * @return object personID
     */
    public String getPersonID() {
        return personID;
    }

    /**
     * Method setPersonID
     *
     * @param personID String to be converted to long
     */
    private void setPersonID(String personID) {
        this.personID = personID;
    }



    /**
     * Method getNewCategory
     *
     * @return object newCategory
     */
    public String designation() {
        return designation;
    }

    /**
     * Method setNewCategory
     *
     * @param designation newCategory of the Category
     */
    private void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateCategoryForPersonResponseDTO that = (CreateCategoryForPersonResponseDTO) o;
        return Objects.equals(personID, that.personID) &&
                Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID, designation);
    }
}
