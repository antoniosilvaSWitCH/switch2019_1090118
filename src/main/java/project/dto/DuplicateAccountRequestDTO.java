package project.dto;

import lombok.Data;

@Data
public class DuplicateAccountRequestDTO {

    private String personID;
    private String accountID;
    private String newAccountID;

    public DuplicateAccountRequestDTO(String personID, String accountID, String newAccountID) {
        this.personID = personID;
        this.accountID = accountID;
        this.newAccountID = newAccountID;
    }

}
